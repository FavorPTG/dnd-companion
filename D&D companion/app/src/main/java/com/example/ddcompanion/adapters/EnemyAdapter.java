package com.example.ddcompanion.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ddcompanion.CharacterList;
import com.example.ddcompanion.R;
import com.example.ddcompanion.Utilities;

import java.util.ArrayList;
import java.util.List;

public class EnemyAdapter extends RecyclerView.Adapter<EnemyAdapter.viewHolder>{

    List<CharacterList> list;

    public EnemyAdapter(List<CharacterList> list){
        this.list = list;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new viewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.enemy_element, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        List<String> l = Utilities.getSkillsList(list.get(position).getForce(), list.get(position).getIntellect(),
                list.get(position).getDexterity(), list.get(position).getBodyType(),
                list.get(position).getWisdom(), list.get(position).getCharisma());

        holder.enemyName.setText(list.get(position).getCharacterName());
        holder.enemyClass.setText(list.get(position).getCharClass());
        holder.enemyRas.setText(list.get(position).getRace());
        holder.enemyIdeology.setText(list.get(position).getIdeology());
        holder.enemyForce.setText(l.get(0));
        holder.enemyIntelligence.setText(l.get(1));
        holder.enemyDexterity.setText(l.get(2));
        holder.enemyBodyType.setText(l.get(3));
        holder.enemyWisdom.setText(l.get(4));
        holder.enemyCharisma.setText(l.get(5));

        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utilities.createDialogMenu(v.getContext(), "Дополнительно о " + list.get(position).getCharacterName()+":", list.get(position).getUniqueFeatures(), null, "Назад").show();
                //Toast.makeText(holder.parent.getContext(), "Типо меню подробной информации о враге", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class viewHolder extends RecyclerView.ViewHolder {
        ViewGroup parent;
        TextView enemyName;
        TextView enemyClass;
        TextView enemyRas;
        TextView enemyIdeology;
        TextView enemyForce;
        TextView enemyIntelligence;
        TextView enemyDexterity;
        TextView enemyBodyType;
        TextView enemyWisdom;
        TextView enemyCharisma;

        public viewHolder(@NonNull View itemView) {
            super(itemView);

            parent = itemView.findViewById(R.id.parent);
            enemyName = itemView.findViewById(R.id.enemyName);
            enemyClass = itemView.findViewById(R.id.enemyClass);
            enemyRas = itemView.findViewById(R.id.enemyRas);
            enemyIdeology = itemView.findViewById(R.id.enemyIdeology);
            enemyForce = itemView.findViewById(R.id.enemyForce);
            enemyIntelligence = itemView.findViewById(R.id.enemyIntelligence);
            enemyDexterity = itemView.findViewById(R.id.enemyDexterity);
            enemyBodyType = itemView.findViewById(R.id.enemyBodyType);
            enemyWisdom = itemView.findViewById(R.id.enemyWisdom) ;
            enemyCharisma = itemView.findViewById(R.id.enemyCharisma);
        }
    }
}
