package com.example.ddcompanion.save;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class SaveCharacterList extends SQLiteOpenHelper {

    public SaveCharacterList(@Nullable Context context) {
        super(context, CHARACTER_LIST_TABLE, null, TABLE_VERSION);
    }

    private static final String CHARACTER_LIST_TABLE = "CHARACTER";
    private static final int TABLE_VERSION = 1;

    private static final String CHARACTER_NAME = "CNAME";
    private static final String CHARACTER_LEVEL = "CLEVEL";
    private static final String RACE = "RACE";
    private static final String CLASS = "CLASS";
    private static final String ORIGIN = "ORIGIN";
    private static final String LANGS = "LANG";
    private static final String UNIQUE = "UNIQUE";
    private static final String BACKSTORY = "BACKSTORY";
    private static final String MAX_HP = "MAXHP";
    private static final String DICE_HP = "DICEHP";
    private static final String FORCE = "FORCE";
    private static final String DEXTERITY = "DEXTERITY";
    private static final String INTELLECT = "INTELLECT";
    private static final String ENDURANCE = "ENDURANCE";
    private static final String WISDOM = "WISDOM";
    private static final String CHARISMA = "CHARISMA";
    private static final String SPEED = "SPEED";
    private static final String ARMOR_CLASS = "ARMORCLASS";
    private static final String ARMOR_NAME = "ARMORNAME";
    private static final String OTHER_ITEMS = "ITEMS";

    private String zap = "";

//    PlayerName = "";
//    CharacterName = "";
//    CharacterLevel = 1;
//    Race = "";
//    Class = "";
//    Origin = "";
//    LanguageProficiency = "";
//    UniqueFeatures = "";
//    BackStory = "";
//    MaxHP = 0;
//    DiceHP = "D6";
//    Force = 0;
//    Dexterity = 0;
//    Intellect = 0;
//    Endurance = 0;
//    Wisdom = 0;
//    Charisma = 0;
//    Speed = 0;
//    ArmorClass = 0;
//    ArmorName = "none";
//    Weapons = null;
//    Spells = null;
//    OtherItems = null;
//    Coins = null;

    public String getTableName() {
        return CHARACTER_LIST_TABLE;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        zap = "CREATE TABLE " + CHARACTER_LIST_TABLE  +
                " (_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                CHARACTER_LEVEL + " TEXT," +
                RACE + " TEXT," +
                ORIGIN + "TEXT" + ")";
        db.execSQL(zap);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        zap = "DROP TABLE IF EXISTS " + CHARACTER_LIST_TABLE;
        db.execSQL(zap);
        onCreate(db);
    }
}
