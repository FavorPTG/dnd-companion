package com.example.ddcompanion.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckedTextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ddcompanion.R;
import com.example.ddcompanion.Utilities;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.viewHolder> {

    private String[] list;
    private Context context;
    private RecyclerView parent;

    public ListAdapter(Context context, String[] list, RecyclerView recyclerView){
        this.context = context;
        this.list = list;
        this.parent = recyclerView;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new viewHolder(LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_multiple_choice, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        holder.checkedTextView.setText(list[position]);

        holder.checkedTextView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                switch(position){
                    case 0:
                        Utilities.createDialogMenu(context, context.getResources().getString(R.string.dwarf_name),
                                context.getResources().getString(R.string.dwarf_desc), null, "Назад").show();
                        break;
                    case 1:
                        Utilities.createDialogMenu(context, context.getResources().getString(R.string.elf_name),
                                context.getResources().getString(R.string.elf_desc), null, "Назад").show();
                        break;
                    case 2:
                        Utilities.createDialogMenu(context, context.getResources().getString(R.string.halfling_name),
                                context.getResources().getString(R.string.hlafling_desc), null, "Назад").show();
                        break;
                    case 3:
                        Utilities.createDialogMenu(context, context.getResources().getString(R.string.human_name),
                                context.getResources().getString(R.string.human_desc), null, "Назад").show();
                        break;
                    case 4:
                        Utilities.createDialogMenu(context, context.getResources().getString(R.string.dragonborn_name),
                                context.getResources().getString(R.string.dragonborn_desc), null, "Назад").show();
                        break;
                    case 5:
                        Utilities.createDialogMenu(context, context.getResources().getString(R.string.gnome_name),
                                context.getResources().getString(R.string.gnome_desc), null, "Назад").show();
                        break;
                    case 6:
                        Utilities.createDialogMenu(context, context.getResources().getString(R.string.half_elf_name),
                                context.getResources().getString(R.string.half_elf_desc), null, "Назад").show();
                        break;
                    case 7:
                        Utilities.createDialogMenu(context, context.getResources().getString(R.string.half_orc_name),
                                context.getResources().getString(R.string.half_orc_desc), null, "Назад").show();
                        break;
                    case 8:
                        Utilities.createDialogMenu(context, context.getResources().getString(R.string.tifling_name),
                                context.getResources().getString(R.string.teifling_desc), null, "Назад").show();
                        break;
                }
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.length;
    }

    public class viewHolder extends RecyclerView.ViewHolder{

        CheckedTextView checkedTextView;

        public viewHolder(@NonNull View itemView) {
            super(itemView);

            checkedTextView = (CheckedTextView) itemView.findViewById(android.R.id.text1);
        }
    }
}
