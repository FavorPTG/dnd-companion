package com.example.ddcompanion.save;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class SaveMaster extends SQLiteOpenHelper {

    public static final String TABLE_ENEMY_NAME = "ENEMY";
    public static final String TABLE_TRAVEL_NAME = "TRAVEL";
    public static final String TABLE_NOTES_NAME = "NOTES";

    private static final String ID = "_id";
    private static final int TABLE_VERSION = 1;

    // Ячейки таблицы для хранения врагов
    private static final String ENEMY_NAME = "NAME";
    private static final String ENEMY_CLASS = "CLASS";
    private static final String ENEMY_RAS = "RAS";
    private static final String ENEMY_IDEOLOGY = "IDEOLOGY";
    private static final String ENEMY_FORCE = "FORCE";
    private static final String ENEMY_INTELLIGENCE = "INTELLIGENCE";
    private static final String ENEMY_DEXTERITY = "DEXTERITY";
    private static final String ENEMY_BODY_TYPE = "BODYTYPE";
    private static final String ENEMY_WISDOM = "WISDOM";
    private static final String ENEMY_CHARISMA = "CHARISMA";
    private static final String ARMOR_CLASS = "ARMOR";
    private static final String WEAPON_NAME = "WEAPON";
    private static final String WEAPON_DAMAGE = "WEAPONDAMAGE";
    private static final String SPELLS = "SPELLS";
    private static final String SHORT_INFO = "INFO";

    // Ячейки таблицы для хранения приключений
    private static final String TRAVEL_NAME = "NAME";
    private static final String TRAVEL_LOCATIONS = "LOCATIONS";
    private static final String TRAVEL_LOCATION_DESCRIPTION = "DESCRIPTION";
    private static final String TRAVEL_NPC = "NPC";

    // Ячейки таблицы для хранения заметок
    private static final String NOTE_LIST = "NOTE";



    public SaveMaster(@Nullable Context context, @Nullable String name) {
        super(context, name, null, TABLE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
//        db.execSQL("CREATE TABLE " + TABLE_ENEMY_NAME  +
//                " (_id INTEGER PRIMARY KEY AUTOINCREMENT," +
//                CHARACTER_LEVEL + " TEXT," +
//                RACE + " TEXT," +
//                ORIGIN + "TEXT" + ")");

        db.execSQL("CREATE TABLE " + TABLE_TRAVEL_NAME +
                " (_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                TRAVEL_NAME + " TEXT," +
                TRAVEL_LOCATIONS + " TEXT," +
                TRAVEL_LOCATION_DESCRIPTION + " TEXT," +
                TRAVEL_NPC + " TEXT)");

        db.execSQL("CREATE TABLE " + TABLE_NOTES_NAME +
                " (_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                NOTE_LIST + " TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRAVEL_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTES_NAME);
        onCreate(db);
    }
}
