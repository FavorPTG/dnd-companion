package com.example.ddcompanion;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.Layout;
import android.util.Log;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.DrawableRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ddcompanion.save.SaveCharacterList;
import com.example.ddcompanion.save.SaveMaster;
import com.example.ddcompanion.ui.character_list.DashboardFragment;

import java.util.ArrayList;
import java.util.List;

public class Utilities {
    private static SaveCharacterList saveCL;  // Ссылка на базу данных листа персонажа
    private static SaveMaster saveMaster;     // Ссылка на базу данных мастера
    private static SQLiteDatabase database;   // База данных
    private static int count = 0;
    private static final int min_value_chars = 1;



    // Методы для создания всплывающих окон
    public static Dialog createFloatMenu(Context context, String info, @LayoutRes int layoutId){
        Dialog dialog  = new Dialog(context);

        dialog.setContentView(layoutId);

//        TextView tv = dialog.findViewById(R.id.textView);
//        tv.setText(info);

        return dialog;
    }
    public static Dialog createDialogMenu(Context context, String header, String text, View.OnClickListener yesBtn, String noName){
        Dialog dialog  = new Dialog(context);

        dialog.setContentView(R.layout.what_need_to_save);

        TextView Header = dialog.findViewById(R.id.header);
        TextView Text = dialog.findViewById(R.id.textView);

        Header.setText(header);
        Text.setText(text);

        Button yes = dialog.findViewById(R.id.yesButton);
        Button no = dialog.findViewById(R.id.noButton);

        if(noName != null){ no.setText(noName); }

        if(yesBtn == null){ yes.setVisibility(View.GONE); }

        yes.setOnClickListener(yesBtn);
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        return dialog;
    }
    public static Dialog createSaveCharacterMenu(Context context, CharacterList cl){
        Dialog dialog  = new Dialog(context);

        dialog.setContentView(R.layout.charecter_list_save_menu);

        TextView info = dialog.findViewById(R.id.info);
        info.setText("Сохранить персонажа?\n\t" + cl.getRace() + "\n\t" + cl.getCharClass() + "\n\t" + cl.getCharacterName());

        Button yesButton = dialog.findViewById(R.id.yesButton);
        Button noButton = dialog.findViewById(R.id.noButton);

        ContentValues cv = new ContentValues();
//        cv.put("name", );

        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // yes
//                Utilities.refactorData(context, );
            }
        });

        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // no
                dialog.dismiss();
            }
        });

        return dialog;
    }
    public static Dialog createIdeologyMenu(Context context, CharacterList cl){
        Dialog dialog  = new Dialog(context);

        dialog.setContentView(R.layout.ideology_menu);

        TextView tv = dialog.findViewById(R.id.current_ideology);

        if(cl.getIdeology() != null){
            tv.setText(cl.getIdeology());
        }

        ImageButton nd = dialog.findViewById(R.id.nd);
        ImageButton zd = dialog.findViewById(R.id.zd);
        ImageButton hd = dialog.findViewById(R.id.xd);
        ImageButton zn = dialog.findViewById(R.id.zn);
        ImageButton hn = dialog.findViewById(R.id.hn);
        ImageButton zz = dialog.findViewById(R.id.zz);
        ImageButton nz = dialog.findViewById(R.id.nz);
        ImageButton hz = dialog.findViewById(R.id.hz);

        // Cлушатели с выводом диалога с информацией о выбранном мировоззрении и подтверждением выбора
        nd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Utilities.createDialogMenu(context, "Нейтрально-добрый", "\tНейтрально-добрые (НД) стараются сделать " +
                        "всё, от них зависящее, чтобы помочь другим. " +
                        "Многие небожители, некоторые облачные великаны и " +
                        "большинство гномов нейтрально-добрые", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cl.setIdeology("Нейтрально-добрый");
                        tv.setText(cl.getIdeology());
                        Toast.makeText(context, "Выбрано мировоззрение: "+cl.getIdeology(), Toast.LENGTH_SHORT).show();
                    }
                }, "Закрыть").show();
            }
        });
        zd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Utilities.createDialogMenu(context, "Законопослушный- добрый", "\tЗаконопослушные-добрые (ЗД) существа совершают " +
                        "поступки, считающиеся в обществе как правильные. " +
                        "Золотые драконы, паладины и большинство " +
                        "дварфов являются законопослушными-добрыми.", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cl.setIdeology("Законопослушный- добрый");
                        tv.setText(cl.getIdeology());
                        Toast.makeText(context, "Выбрано мировоззрение: "+cl.getIdeology(), Toast.LENGTH_SHORT).show();
                    }
                }, "Закрыть").show();
            }
        });
        hd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Utilities.createDialogMenu(context, "Хаотично-добрый", "\tХаотично-добрые (ХД) существа действуют по " +
                        "собственной совести, вне зависимости от того, что " +
                        "думают другие. Медные драконы, многие эльфы и " +
                        "единороги хаотично-добрые.", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cl.setIdeology("Хаотично-добрый");
                        tv.setText(cl.getIdeology());
                        Toast.makeText(context, "Выбрано мировоззрение: "+cl.getIdeology(), Toast.LENGTH_SHORT).show();
                    }
                }, "Закрыть").show();
            }
        });
        zn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Utilities.createDialogMenu(context, "Законопослушный- нейтральный", "\tЗаконопослушные-нейтральные (ЗН) индивидуумы " +
                        "действуют в соответствии с законом, традицией, или " +
                        "личным кодексом. Многие монахи и некоторые " +
                        "волшебники являются законопослушными- нейтральными.", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cl.setIdeology("Законопослушный- нейтральный");
                        tv.setText(cl.getIdeology());
                        Toast.makeText(context, "Выбрано мировоззрение: "+cl.getIdeology(), Toast.LENGTH_SHORT).show();
                    }
                }, "Закрыть").show();
            }
        });
        hn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Utilities.createDialogMenu(context, "Хаотично- нейтральный", "\tХаотично-нейтральные (ХН) существа следуют " +
                        "своим прихотям, держа свою личную свободу " +
                        "превыше всего. Многие варвары и разбойники, а " +
                        "также некоторые барды хаотично-нейтральны.", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cl.setIdeology("Хаотично- нейтральный");
                        tv.setText(cl.getIdeology());
                        Toast.makeText(context, "Выбрано мировоззрение: "+cl.getIdeology(), Toast.LENGTH_SHORT).show();
                    }
                }, "Закрыть").show();
            }
        });
        zz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Utilities.createDialogMenu(context, "Законопослушный- злой", "\tЗаконопослушные-злые (ЗЗ) существа методично берут " +
                        "то, что хотят, в рамках кодекса традиции, " +
                        "верности, или порядка. Дьяволы, синие драконы и " +
                        "хобгоблины законопослушные-злые.", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cl.setIdeology("Законопослушный- злой");
                        tv.setText(cl.getIdeology());
                        Toast.makeText(context, "Выбрано мировоззрение: "+cl.getIdeology(), Toast.LENGTH_SHORT).show();
                    }
                }, "Закрыть").show();
            }
        });
        nz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Utilities.createDialogMenu(context, "Нейтрально-злой", "\tНейтрально-злое (НЗ) мировоззрение у тех, кто " +
                        "делает всё, что может сойти им с рук, без " +
                        "сострадания и угрызений совести. Многие дроу, некоторые " +
                        "облачные великаны и юголоты нейтрально-злы.", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cl.setIdeology("Нейтрально-злой");
                        tv.setText(cl.getIdeology());
                        Toast.makeText(context, "Выбрано мировоззрение: "+cl.getIdeology(), Toast.LENGTH_SHORT).show();
                    }
                }, "Закрыть").show();
            }
        });
        hz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Utilities.createDialogMenu(context, "Хаотично-злой", "\tХаотично-злые (ХЗ) существа действуют со " +
                        "спонтанной жестокостью, вызванной их " +
                        "жадностью, ненавистью или жаждой крови. Демоны, " +
                        "красные драконы и орки хаотично-злы.", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cl.setIdeology("Хаотично-злой");
                        tv.setText(cl.getIdeology());
                        Toast.makeText(context, "Выбрано мировоззрение: "+cl.getIdeology(), Toast.LENGTH_SHORT).show();
                    }
                }, "Закрыть").show();
            }
        });

        return dialog;
    }
    public static Dialog createPlacesMenu(Context context, @LayoutRes int layout, String header, String et1, String et2){
        Dialog dialog = new Dialog(context);

        if(layout == 0) {
            dialog.setContentView(R.layout.new_place_dialog_menu);
        } else
            dialog.setContentView(layout);

        TextView head = dialog.findViewById(R.id.header);
        EditText name = dialog.findViewById(R.id.locName);
        EditText duration = dialog.findViewById(R.id.duration);

        if(header != null)
            head.setText(header);
        if(et1 != null)
            name.setHint(et1);
        if(et2 != null)
            duration.setHint(et2);

        Button yesButton = dialog.findViewById(R.id.yesButton);
        Button noButton = dialog.findViewById(R.id.noButton);

//        yesButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                list += name.getText() + "\n\t" + duration.getText() + "\n\n";
//                dialog.dismiss();
//                list.add(new Pair<>(name.getText().toString(), duration.getText().toString()));
//            }
//        });

        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        return dialog;
    }
    public static Dialog createNewEnemy(Context context, List<CharacterList> enemyList, RecyclerView rv){
        Dialog dialog = new Dialog(context);
        CharacterList el = new CharacterList();
        final int min_value_chars = 1;

        dialog.setContentView(R.layout.new_enemy_dialog_menu);

        Button  // Кнопки "плюс"
                forcePls = dialog.findViewById(R.id.force_plus_btn),
                dexPls = dialog.findViewById(R.id.dexterity_plus_btn),
                intPls = dialog.findViewById(R.id.intelligece_plus_btn),
                endurancePls = dialog.findViewById(R.id.endurance_plus_btn),
                wisdomPls = dialog.findViewById(R.id.wisdom_plus_btn),
                charismaPls = dialog.findViewById(R.id.charisma_plus_btn),
                // Кнопки "минус"
                forceInc = dialog.findViewById(R.id.force_inc_btn),
                dexInc = dialog.findViewById(R.id.dexterity_inc_btn),
                intInc = dialog.findViewById(R.id.intelligece_inc_btn),
                enduranceInc = dialog.findViewById(R.id.endurance_inc_btn),
                wisdomInc = dialog.findViewById(R.id.wisdom_inc_btn),
                charismaInc = dialog.findViewById(R.id.charisma_inc_btn),
                // Кнопки "да" и "нет"
                yesButton = dialog.findViewById(R.id.yesButton),
                noButton = dialog.findViewById(R.id.noButton);

        ImageButton
                rasMenuButton = dialog.findViewById(R.id.enemyRasMenu),
                classMenuButton = dialog.findViewById(R.id.enemyClassMenu),
                ideologyMenuButton = dialog.findViewById(R.id.enemyIdeologyMenu);

        EditText
                enemyName = dialog.findViewById(R.id.enemyName),
                forceText = dialog.findViewById(R.id.force_text),
                dexText = dialog.findViewById(R.id.dexterity_text),
                intText = dialog.findViewById(R.id.intelligece_text),
                enduranceText = dialog.findViewById(R.id.endurance_text),
                wisdomText = dialog.findViewById(R.id.wisdom_text),
                charismaText = dialog.findViewById(R.id.charisma_text),
                additionInfoText = dialog.findViewById(R.id.addition_info_text);

        LinearLayout
                rasContainer = dialog.findViewById(R.id.ras_container),
                classContainer = dialog.findViewById(R.id.class_container);

        // Обработка кнопок плюс-минус
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    // Обработка кнопок "минус"
                    case R.id.force_inc_btn:
                        el.setForce(Math.max(el.getForce()-1, min_value_chars));
                        forceText.setText(el.getForce()+"");
                        break;
                    case R.id.dexterity_inc_btn:
                        el.setDexterity(Math.max(el.getDexterity()-1, min_value_chars));
                        dexText.setText(el.getDexterity()+"");
                        break;
                    case R.id.intelligece_inc_btn:
                        el.setIntellect(Math.max(el.getIntellect()-1, min_value_chars));
                        intText.setText(el.getIntellect()+"");
                        break;
                    case R.id.endurance_inc_btn:
                        el.setBodyType(Math.max(el.getBodyType()-1, min_value_chars));
                        enduranceText.setText(el.getBodyType()+"");
                        break;
                    case R.id.wisdom_inc_btn:
                        el.setWisdom(Math.max(el.getWisdom()-1, min_value_chars));
                        wisdomText.setText(el.getWisdom()+"");
                        break;
                    case R.id.charisma_inc_btn:
                        el.setCharisma(Math.max(el.getCharisma()-1, min_value_chars));
                        charismaText.setText(el.getCharisma()+"");
                        break;

                    // Обработка кнопок "плюс"
                    case R.id.force_plus_btn:
                        el.setForce(Math.max(el.getForce()+1, min_value_chars));
                        forceText.setText(el.getForce()+"");
                        break;
                    case R.id.dexterity_plus_btn:
                        el.setDexterity(Math.max(el.getDexterity()+1, min_value_chars));
                        dexText.setText(el.getDexterity()+"");
                        break;
                    case R.id.intelligece_plus_btn:
                        el.setIntellect(Math.max(el.getIntellect()+1, min_value_chars));
                        intText.setText(el.getIntellect()+"");
                        break;
                    case R.id.endurance_plus_btn:
                        el.setBodyType(Math.max(el.getBodyType()+1, min_value_chars));
                        enduranceText.setText(el.getBodyType()+"");
                        break;
                    case R.id.wisdom_plus_btn:
                        el.setWisdom(Math.max(el.getWisdom()+1, min_value_chars));
                        wisdomText.setText(el.getWisdom()+"");
                        break;
                    case R.id.charisma_plus_btn:
                        el.setCharisma(Math.max(el.getCharisma()+1, min_value_chars));
                        charismaText.setText(el.getCharisma()+"");
                        break;
                }
            }
        };

        rasMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utilities.animate(rasContainer);
            }
        });
        classMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utilities.animate(classContainer);
            }
        });

        // Выделение расы
        List<CheckBox> cbList = new ArrayList<>();
        for(int i=0; i<rasContainer.getChildCount(); i++){
            if(rasContainer.getChildAt(i) instanceof LinearLayout){
                LinearLayout l = (LinearLayout) rasContainer.getChildAt(i);
                for(int j=0; j<l.getChildCount(); j++){
                    if(l.getChildAt(j) instanceof CheckBox){
                        CheckBox checkBox = (CheckBox) l.getChildAt(j);
                        cbList.add(checkBox);
                    }
                }
            }
        }
        for(int i=0; i<cbList.size(); i++){
            int finalI = i;
            cbList.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(el.getRace() == null || el.getRace() == ""){
                        for(int j=0; j<cbList.size(); j++){
                            cbList.get(j).setEnabled(false);
                            cbList.get(j).setChecked(false);
                        }
                        cbList.get(finalI).setEnabled(true);
                        cbList.get(finalI).setChecked(true);
                        el.setRace(cbList.get(finalI).getTag().toString());
                    } else if(el.getRace() != null && cbList.get(finalI).isEnabled()){
                        for(int j=0; j<cbList.size(); j++){
                            cbList.get(j).setEnabled(true);
                            cbList.get(j).setChecked(false);
                        }
                        el.setRace(null);
                    }
                }
            });
        }

        // Выделение класса
        List<CheckBox> classCbList = new ArrayList<>();
        for(int i=0; i<classContainer.getChildCount(); i++){
            if(classContainer.getChildAt(i) instanceof LinearLayout){
                LinearLayout l = (LinearLayout) classContainer.getChildAt(i);
                for(int j=0; j<l.getChildCount(); j++){
                    if(l.getChildAt(j) instanceof CheckBox){
                        CheckBox checkBox = (CheckBox) l.getChildAt(j);
                        classCbList.add(checkBox);
                    }
                }
            }
        }
        for(int i=0; i<classCbList.size(); i++){
            int finalI = i;
            classCbList.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(el.getCharClass() == null || el.getCharClass() == ""){
                        for(int j=0; j<classCbList.size(); j++){
                            classCbList.get(j).setEnabled(false);
                            classCbList.get(j).setChecked(false);
                        }
                        classCbList.get(finalI).setEnabled(true);
                        classCbList.get(finalI).setChecked(true);
                        el.setClass(classCbList.get(finalI).getTag().toString());
                    } else if(el.getCharClass() != null && classCbList.get(finalI).isEnabled()){
                        for(int j=0; j<classCbList.size(); j++){
                            classCbList.get(j).setEnabled(true);
                            classCbList.get(j).setChecked(false);
                        }
                        el.setClass(null);
                    }
                }
            });
        }

        // Выбор мировоззрения
        ideologyMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utilities.createIdeologyMenu(context, el).show();
            }
        });

        forcePls.setOnClickListener(listener);
        dexPls.setOnClickListener(listener);
        intPls.setOnClickListener(listener);
        endurancePls.setOnClickListener(listener);
        wisdomPls.setOnClickListener(listener);
        charismaPls.setOnClickListener(listener);

        forceInc.setOnClickListener(listener);
        dexInc.setOnClickListener(listener);
        intInc.setOnClickListener(listener);
        enduranceInc.setOnClickListener(listener);
        wisdomInc.setOnClickListener(listener);
        charismaInc.setOnClickListener(listener);

        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                el.setCharacterName(enemyName.getText().toString());
                el.setUniqueFeatures(additionInfoText.getText().toString());
                enemyList.add(el);
                rv.getAdapter().notifyDataSetChanged();
                Toast.makeText(context, "Добавлен новая сущность : "+el.getCharacterName(), Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        return dialog;
    }
    public static Dialog createNewCharacterMenu(Context context, CharacterList cl){
        Dialog dialog = new Dialog(context);

        dialog.setContentView(R.layout.new_character_menu_layout);


        // Заполнение списка характеристик (ListView)
        String[] chars = context.getResources().getStringArray(R.array.characteristics);
        ListView charsList = dialog.findViewById(R.id.characteristicsList);
        ArrayAdapter<String> charsAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_multiple_choice, chars);
        charsList.setAdapter(charsAdapter);
        charsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CheckedTextView ct = (CheckedTextView) charsList.getChildAt(position);
                if(ct.isChecked()){
                    ct.setChecked(false);
                    count--;
                } else if(!ct.isChecked() && count <= cl.getNumberOfFeaturesAvailable() && ct.isEnabled()){
//                    Toast.makeText(context, count+"", Toast.LENGTH_SHORT).show();
                    ct.setChecked(true);
                    count++;
                }
            }
        });

        // Обработчик кнопок плюс-минус
        View.OnClickListener statsListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView t;
                switch(v.getId()){
                    // Книпки плюс
                    case R.id.force_plus_btn:
                        cl.setForce(Math.max((cl.getForce()+1), min_value_chars));
                        t = dialog.findViewById(R.id.force_text);
                        t.setText(cl.getForce()+"");
                        break;
                    case R.id.dexterity_plus_btn:
                        cl.setDexterity(Math.max((cl.getDexterity()+1), min_value_chars));
                        t = dialog.findViewById(R.id.dexterity_text);
                        t.setText(cl.getDexterity()+"");
                        break;
                    case R.id.intelligece_plus_btn:
                        cl.setIntellect(Math.max((cl.getIntellect()+1), min_value_chars));
                        t = dialog.findViewById(R.id.intelligece_text);
                        t.setText(cl.getIntellect()+"");
                        break;
                    case R.id.endurance_plus_btn:
                        cl.setBodyType(Math.max((cl.getBodyType()+1), min_value_chars));
                        t = dialog.findViewById(R.id.endurance_text);
                        t.setText(cl.getBodyType()+"");
                        break;
                    case R.id.charisma_plus_btn:
                        cl.setCharisma(Math.max((cl.getCharisma()+1), min_value_chars));
                        t = dialog.findViewById(R.id.charisma_text);
                        t.setText(cl.getCharisma()+"");
                        break;
                    case R.id.wisdom_plus_btn:
                        cl.setWisdom(Math.max((cl.getWisdom()+1), min_value_chars));
                        t = dialog.findViewById(R.id.wisdom_text);
                        t.setText(cl.getWisdom()+"");
                        break;

                    // Кнопки минус
                    case R.id.force_inc_btn:
                        cl.setForce(Math.max((cl.getForce()-1), min_value_chars));
                        t = dialog.findViewById(R.id.force_text);
                        t.setText(cl.getForce()+"");
                        break;
                    case R.id.dexterity_inc_btn:
                        cl.setDexterity(Math.max((cl.getDexterity()-1), min_value_chars));
                        t = dialog.findViewById(R.id.dexterity_text);
                        t.setText(cl.getDexterity()+"");
                        break;
                    case R.id.intelligece_inc_btn:
                        cl.setIntellect(Math.max((cl.getIntellect()-1), min_value_chars));
                        t = dialog.findViewById(R.id.intelligece_text);
                        t.setText(cl.getIntellect()+"");
                        break;
                    case R.id.endurance_inc_btn:
                        cl.setBodyType(Math.max((cl.getBodyType()-1), min_value_chars));
                        t = dialog.findViewById(R.id.endurance_text);
                        t.setText(cl.getBodyType()+"");
                        break;
                    case R.id.charisma_inc_btn:
                        cl.setCharisma(Math.max((cl.getCharisma()-1), min_value_chars));
                        t = dialog.findViewById(R.id.charisma_text);
                        t.setText(cl.getCharisma()+"");
                        break;
                    case R.id.wisdom_inc_btn:
                        cl.setWisdom(Math.max((cl.getWisdom() - 1), min_value_chars));
                        t = dialog.findViewById(R.id.wisdom_text);
                        t.setText(cl.getWisdom()+"");
                        break;
                }
            }
        };
        // Установка слушателей "плюс"
        dialog.findViewById(R.id.force_plus_btn).setOnClickListener(statsListener);
        dialog.findViewById(R.id.dexterity_plus_btn).setOnClickListener(statsListener);
        dialog.findViewById(R.id.intelligece_plus_btn).setOnClickListener(statsListener);
        dialog.findViewById(R.id.endurance_plus_btn).setOnClickListener(statsListener);
        dialog.findViewById(R.id.charisma_plus_btn).setOnClickListener(statsListener);
        dialog.findViewById(R.id.wisdom_plus_btn).setOnClickListener(statsListener);
        // Установка слушателей "минус"
        dialog.findViewById(R.id.force_inc_btn).setOnClickListener(statsListener);
        dialog.findViewById(R.id.dexterity_inc_btn).setOnClickListener(statsListener);
        dialog.findViewById(R.id.intelligece_inc_btn).setOnClickListener(statsListener);
        dialog.findViewById(R.id.endurance_inc_btn).setOnClickListener(statsListener);
        dialog.findViewById(R.id.charisma_inc_btn).setOnClickListener(statsListener);
        dialog.findViewById(R.id.wisdom_inc_btn).setOnClickListener(statsListener);

        // Обработка дажатия Enter
        View.OnKeyListener enterKeyListener = new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                EditText t;
                if(event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER){
                    switch (v.getId()){
                        case R.id.force_text:
                            t = dialog.findViewById(R.id.force_text);
                            cl.setForce(Integer.parseInt(t.getText().toString()));
                            break;
                        case R.id.dexterity_text:
                            t = dialog.findViewById(R.id.dexterity_text);
                            cl.setDexterity(Integer.parseInt(t.getText().toString()));
                            break;
                        case R.id.intelligece_text:
                            t = dialog.findViewById(R.id.intelligece_text);
                            cl.setIntellect(Integer.parseInt(t.getText().toString()));
                            break;
                        case R.id.endurance_text:
                            t = dialog.findViewById(R.id.endurance_text);
                            cl.setBodyType(Integer.parseInt(t.getText().toString()));
                            break;
                        case R.id.charisma_text:
                            t = dialog.findViewById(R.id.charisma_text);
                            cl.setCharisma(Integer.parseInt(t.getText().toString()));
                            break;
                        case R.id.wisdom_text:
                            t = dialog.findViewById(R.id.wisdom_text);
                            cl.setWisdom(Integer.parseInt(t.getText().toString()));
                            break;
                    }
                }
                return false;
            }
        };
        dialog.findViewById(R.id.force_text).setOnKeyListener(enterKeyListener);
        dialog.findViewById(R.id.dexterity_text).setOnKeyListener(enterKeyListener);
        dialog.findViewById(R.id.intelligece_text).setOnKeyListener(enterKeyListener);
        dialog.findViewById(R.id.endurance_text).setOnKeyListener(enterKeyListener);
        dialog.findViewById(R.id.charisma_text).setOnKeyListener(enterKeyListener);
        dialog.findViewById(R.id.wisdom_text).setOnKeyListener(enterKeyListener);

        // Обработчики кнопок-меню
        View.OnClickListener menuButtonsListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch(v.getId()){
                    case R.id.spinner_ras:
                        raceMenu(context, v, cl);
                        break;
                    case R.id.spinner_class:
                        classMenu(context, v, cl);
                        break;
                    case R.id.spinner_backstory:
                        backstoryMenu(context, v, cl);
                        break;
                    case R.id.spinner_armor:
                        armorMenu(context, v, cl);
                        break;
                    case R.id.spinner_weapons:
                        weaponMenu(context, v, cl);
                        break;
                    case R.id.spinner_langs:
                        langMenu(context, v, cl);
                        break;
                    case R.id.spinner_ideology:
                        Utilities.createIdeologyMenu(context, cl).show();
                        break;
                    case R.id.menu_chars_button:
                        Utilities.animate(dialog.findViewById(R.id.chars_menu));
                        break;
                }
            }
        };
        dialog.findViewById(R.id.spinner_ras).setOnClickListener(menuButtonsListener);
        dialog.findViewById(R.id.spinner_class).setOnClickListener(menuButtonsListener);
        dialog.findViewById(R.id.spinner_backstory).setOnClickListener(menuButtonsListener);
        dialog.findViewById(R.id.spinner_armor).setOnClickListener(menuButtonsListener);
        dialog.findViewById(R.id.spinner_weapons).setOnClickListener(menuButtonsListener);
        dialog.findViewById(R.id.spinner_langs).setOnClickListener(menuButtonsListener);
        dialog.findViewById(R.id.spinner_ideology).setOnClickListener(menuButtonsListener);
        dialog.findViewById(R.id.menu_chars_button).setOnClickListener(menuButtonsListener);

        View.OnClickListener diceListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView diceText = dialog.findViewById(R.id.diceText);
                switch (v.getId()){
                    case R.id.throwD4:
                        diceText.setText(Utilities.rdm(1, 5));
                        break;
                    case R.id.throwD6:
                        diceText.setText(Utilities.rdm(1, 7));
                        break;
                    case R.id.throwD10:
                        diceText.setText(Utilities.rdm(1, 11));
                        break;
                    case R.id.throwD12:
                        diceText.setText(Utilities.rdm(1, 13));
                        break;
                    case R.id.throwD20:
                        diceText.setText(Utilities.rdm(1, 21));
                        break;
                    case R.id.throwD100:
                        diceText.setText(Utilities.rdm(1, 101));
                        break;
                    default:
                        diceText.setText("Error");
                }
            }
        };
        dialog.findViewById(R.id.throwD4).setOnClickListener(diceListener);
        dialog.findViewById(R.id.throwD6).setOnClickListener(diceListener);
        dialog.findViewById(R.id.throwD10).setOnClickListener(diceListener);
        dialog.findViewById(R.id.throwD12).setOnClickListener(diceListener);
        dialog.findViewById(R.id.throwD20).setOnClickListener(diceListener);
        dialog.findViewById(R.id.throwD100).setOnClickListener(diceListener);

        ImageView menuButton = (ImageView) dialog.findViewById(R.id.menuButton);
        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utilities.animate(dialog.findViewById(R.id.rightMenu));
            }
        });

        Button create = dialog.findViewById(R.id.create);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cl.getRace() == null || cl.getRace().isEmpty()) {
                    Toast.makeText(context, "Не заполнено поле расы...", Toast.LENGTH_LONG).show();
                    return;
                }
                else if(cl.getCharacterClass() == null || cl.getCharacterClass().isEmpty()) {
                    Toast.makeText(context, "Не заполнено поле класса...", Toast.LENGTH_LONG).show();
                    return;
                }
                else if(cl.getOrigin() == null || cl.getOrigin().isEmpty()) {
                    Toast.makeText(context, "Не заполнено поле предыстории...", Toast.LENGTH_LONG).show();
                    return;
                }
//                else if(cl.getArmorClass() == 0) {
//                    Toast.makeText(getContext(), "Не не заполнено поле доспехов...", Toast.LENGTH_LONG).show();
//                    return;
//                }
                else if(cl.getLanguageProficiency() == null || cl.getLanguageProficiency().isEmpty()){
                    Toast.makeText(context, "Не заполнено поле языков...", Toast.LENGTH_LONG).show();
                    return;
                }
                else if(cl.getIdeology() == null || cl.getIdeology().isEmpty()) {
                    Toast.makeText(context, "Не заполнено поле идеологии...", Toast.LENGTH_LONG).show();
                    return;
                }

//    private int Force;     // Сила
//    private int Dexterity; // Ловкость
//    private int Intellect; // Интеллект
//    private int BodyType/endurance;  // Телосложение
//    private int Wisdom;    // Мудрость
//    private int Charisma;  // Харизма

                EditText forceText = dialog.findViewById(R.id.force_text);
                EditText dexterityText = dialog.findViewById(R.id.dexterity_text);
                EditText intelligeceText = dialog.findViewById(R.id.intelligece_text);
                EditText enduranceText = dialog.findViewById(R.id.endurance_text);
                EditText wisdomText = dialog.findViewById(R.id.wisdom_text);
                EditText charismaText = dialog.findViewById(R.id.charisma_text);
                EditText characterName = dialog.findViewById(R.id.characterName);
                EditText skillsAndFeatures = dialog.findViewById(R.id.skillsAndFeatures);
                cl.setForce(Integer.parseInt(forceText.getText().toString()));
                cl.setDexterity(Integer.parseInt(dexterityText.getText().toString()));
                cl.setIntellect(Integer.parseInt(intelligeceText.getText().toString()));
                cl.setBodyType(Integer.parseInt(enduranceText.getText().toString()));
                cl.setWisdom(Integer.parseInt(wisdomText.getText().toString()));
                cl.setCharisma(Integer.parseInt(charismaText.getText().toString()));

                CheckedTextView ctv;
                ctv = (CheckedTextView) charsList.getChildAt(0);
                cl.setAcrobaticPossession(ctv.isChecked());
                ctv = (CheckedTextView) charsList.getChildAt(1);
                cl.setAnalisePossession(ctv.isChecked());
                ctv = (CheckedTextView) charsList.getChildAt(2);
                cl.setAthleticsPossession(ctv.isChecked());
                ctv = (CheckedTextView) charsList.getChildAt(3);
                cl.setAttentivenessPossession(ctv.isChecked());
                ctv = (CheckedTextView) charsList.getChildAt(4);
                cl.setSurvivalPossession(ctv.isChecked());
                ctv = (CheckedTextView) charsList.getChildAt(5);
                cl.setPerformancePossession(ctv.isChecked());
                ctv = (CheckedTextView) charsList.getChildAt(6);
                cl.setIntimidationPossession(ctv.isChecked());
                ctv = (CheckedTextView) charsList.getChildAt(7);
                cl.setHistoryPossession(ctv.isChecked());
                ctv = (CheckedTextView) charsList.getChildAt(8);
                cl.setSleightOfHandPossession(ctv.isChecked());
                ctv = (CheckedTextView) charsList.getChildAt(9);
                cl.setMagickPossession(ctv.isChecked());
                ctv = (CheckedTextView) charsList.getChildAt(10);
                cl.setMedicinePossession(ctv.isChecked());
                ctv = (CheckedTextView) charsList.getChildAt(11);
                cl.setDeceptionPossession(ctv.isChecked());
                ctv = (CheckedTextView) charsList.getChildAt(12);
                cl.setNaturePossession(ctv.isChecked());
                ctv = (CheckedTextView) charsList.getChildAt(13);
                cl.setInsightPossession(ctv.isChecked());
                ctv = (CheckedTextView) charsList.getChildAt(14);
                cl.setReligionPossession(ctv.isChecked());
                ctv = (CheckedTextView) charsList.getChildAt(15);
                cl.setSecrecyPossession(ctv.isChecked());
                ctv = (CheckedTextView) charsList.getChildAt(16);
                cl.setBeliefPossession(ctv.isChecked());
                ctv = (CheckedTextView) charsList.getChildAt(17);
                cl.setAnimalCarePossession(ctv.isChecked());

                cl.setCharacterName(characterName.getText().toString());
                cl.setUniqueFeatures(skillsAndFeatures.getText().toString());

                cl.compileCharacterList();

//                CharacterName;  // Имя персонажа
//                Race;           // Раса персонажа
//                Class;          // Класс персонажа
//                Origin;         // Происхождение персонажа
//                ideology;       // Мировоззрение персонажа
//
//                // Заполняемые игроком поля
//                LanguageProficiency; // Владение языками
//                UniqueFeatures;      // Уникальные особенности
//                BackStory;           // Предыстория персонажа
//
//                // Здоровье персонажа
//                MaxHP;        // Максимальные хиты
//                CurrentHP;    // Текущие хиты
//                TemporaryHP;  // Временные хиты
//                DiceHP;       // Кость хитов
//
//                // Модификаторы характеристик персонажа
//                ForceMod;     // Модификатор силы
//                DexterityMod; // Модификатор ловкости
//                IntellectMod; // Модификатор интеллекта
//                EnduranceMod; // Модификатор телосложения
//                WisdomMod;    // Модификатор мудрости
//                CharismaMod;  // Модификатор харизмы
//
//                // Спасброски
//                private int ForceSaveMod;     // Модификатор силы
//                private int DexteritySaveMod; // Модификатор ловкости
//                private int IntellectSaveMod; // Модификатор интеллекта
//                private int EnduranceSaveMod; // Модификатор выносливости
//                private int WisdomSaveMod;    // Модификатор мудрости
//                private int CharismaSaveMod;  // Модификатор харизмы
//
//                // Навыки
//                private int acrobatic;      // Акробатика
//                private int analise;        // Анализ
//                private int athletics;      // Атлетика
//                private int attentiveness;  // Внимательность
//                private int survival;       // Выживание
//                private int performance;    // Выступление
//                private int intimidation;   // Запугивание
//                private int history;        // История
//                private int sleightOfHand;  // Ловкость рук
//                private int magick;         // Магия
//                private int medicine;       // Медицина
//                private int deception;      // Обман
//                private int nature;         // Природа
//                private int insight;        // Проницательность
//                private int religion;       // Религия
//                private int secrecy;        // Скрытность
//                private int belief;         // Убеждение
//                private int animalCare;     // Уход за животными
//
//                // Второстепенные пункты
//                private int Inspiration;        // Вдохновение
//                private int SkillBonus;         // Бонус мастерства
//                private int PassiveMindfulness; // Пассивная внимательность
//                private int Speed;              // Скорость перемещения в футах (одна клетка = 5 футов)

                if(charsList.getAdapter() != null){
                    for(int i=0; i<chars.length; i++){
                        charsList.getChildAt(i).setEnabled(false);
                    }
                    cl.compileCharacterList();
                    charsList.getChildAt(0).setEnabled(cl.isAcrobaticPossession());
                    charsList.getChildAt(1).setEnabled(cl.isAnalisePossession());
                    charsList.getChildAt(2).setEnabled(cl.isAthleticsPossession());
                    charsList.getChildAt(3).setEnabled(cl.isAttentivenessPossession());
                    charsList.getChildAt(4).setEnabled(cl.isSurvivalPossession());
                    charsList.getChildAt(5).setEnabled(cl.isPerformancePossession());
                    charsList.getChildAt(6).setEnabled(cl.isIntimidationPossession());
                    charsList.getChildAt(7).setEnabled(cl.isHistoryPossession());
                    charsList.getChildAt(8).setEnabled(cl.isSleightOfHandPossession());
                    charsList.getChildAt(9).setEnabled(cl.isMagickPossession());
                    charsList.getChildAt(10).setEnabled(cl.isMedicinePossession());
                    charsList.getChildAt(11).setEnabled(cl.isDeceptionPossession());
                    charsList.getChildAt(12).setEnabled(cl.isNaturePossession());
                    charsList.getChildAt(13).setEnabled(cl.isInsightPossession());
                    charsList.getChildAt(14).setEnabled(cl.isReligionPossession());
                    charsList.getChildAt(15).setEnabled(cl.isSecrecyPossession());
                    charsList.getChildAt(16).setEnabled(cl.isBeliefPossession());
                    charsList.getChildAt(17).setEnabled(cl.isAnimalCarePossession());
                }

                //

                cl.setWantToUpdate(true);
                DashboardFragment.executeTesk();
                dialog.dismiss();
            }
        });

        return dialog;
    }

    // Метод для создания анимации исчезновения/появления
    public static void animate(View view){
        int duration = 200;
        boolean isFadeOut = view.getVisibility() == View.GONE;

        if(isFadeOut) {
            view.setAlpha(0f);
            view.animate().alpha(1f).setDuration(duration).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(@NonNull Animator animation) {
                    //if(view.getVisibility() != View.VISIBLE)
                    view.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(@NonNull Animator animation) {
                    //
                }

                @Override
                public void onAnimationCancel(@NonNull Animator animation) {
                    //
                }

                @Override
                public void onAnimationRepeat(@NonNull Animator animation) {
                    //
                }
            });
        } else {
            view.setAlpha(1f);
            view.animate().alpha(0f).setDuration(duration).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(@NonNull Animator animation) {
                    view.setAlpha(1f);
                }

                @Override
                public void onAnimationEnd(@NonNull Animator animation) {
                    view.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationCancel(@NonNull Animator animation) {
                    //
                }

                @Override
                public void onAnimationRepeat(@NonNull Animator animation) {
                    //
                }
            });
        }
    }

    // Метод для сохранения данных
    public static void saveData(int screen){
        // TODO: Кажется, этот метод должен обобщать методы для БД
        switch (screen){
            case 1:
                //
                break;
            case 2:
                //
                break;
            case 3:
                //
                break;
        }
    }

    // Метод для добавления строки в таблицу
    public static int refactorData(Context context, ContentValues cv){
        if(saveCL == null) {
            saveCL = new SaveCharacterList(context);   // TODO: Сейчас читается только таблица листа персонажа. Нужно придумать, как читать любую базу данных
            database = saveCL.getWritableDatabase();
        }

        long res = database.insert(saveCL.getTableName(), null, cv);

        if(res == 0)
            Toast.makeText(context, "Данные успешно сохранены", Toast.LENGTH_LONG).show();
        else
            Toast.makeText(context, "Ошибка при сохранении...", Toast.LENGTH_LONG).show();

        return 0;
    }

    // Метод для чтения данных из таблиц
    public static Cursor readData(Context context){
        if(saveCL == null) {
            saveCL = new SaveCharacterList(context);
            database = saveCL.getWritableDatabase();
        }

        Cursor cursor = database.query(saveCL.getTableName(), null, null, null, null, null, null);

        // cursor.getCount();          Вернёт количество рядов
        //cursor.getColumnIndex(name)  Вернёт индекс столбца по его имени
        // cursor.getColumnCount()     Вернёт количество колонок
        // cursor.moveToPosition(X)    Переместит курсор на ряд X
        // cursor.getString(X)         Вернёт строковое значение из данного ряда по индексу X

//        if (cursor.moveToFirst()) {
//            int nameIndex = cursor.getColumnIndex(saveCL.KEY_NAME);
//            int emailIndex = cursor.getColumnIndex(saveCL.KEY_MAIL);
//            do {
//                Log.d("mLog",
//                        ", name = " + cursor.getString(nameIndex) +
//                        ", email = " + cursor.getString(emailIndex));
//            } while (cursor.moveToNext());
//        } else
//            Log.d("mLog","0 rows");
//
//        cursor.close();

        return cursor;
    }

    // Метод для полного удаления таблиы
    public static int deleteTable(Context context){
        // TODO: Метод работает только с таблицей для листа персонажа
        if(saveCL == null) {
            saveCL = new SaveCharacterList(context);
            database = saveCL.getWritableDatabase();
        }

        return database.delete(saveCL.getTableName(), null, null);
    }

    // Метод для удаления строки таблицы
    public static int deleteTableRow(Context context, int id){
        // TODO: Метод работает только с таблицей для листа персонажа
        if(saveCL == null) {
            saveCL = new SaveCharacterList(context);
            database = saveCL.getWritableDatabase();
        }

        return database.delete(saveCL.getTableName(), "_id="+id, null);
    }

    // Метод возвращающий количество колонок в заданной таблице
    public static int getColumnOfTable(Context context){
        // TODO: Метод работает только с таблицей для листа персонажа
        if(saveCL == null) {
            saveCL = new SaveCharacterList(context);
            database = saveCL.getWritableDatabase();
        }

        Cursor cursor = database.query(saveCL.getTableName(), null, null, null, null, null, null);

        return cursor.getColumnCount();
    }


    private static final String MASTER_TABLE = "MASTER";

    // Метод вернёт имя таблицы (1-лист персонжа, 2-утилиты мастера)
    public static String getTableName(int index){
        return index == 1 ? saveCL.getTableName() : MASTER_TABLE;
    }



    // Метод для определения модификаторов характеристик
    public static int getMods(int i){
        if(i == 1)
            return -5;
        else if(i==2 || i==3)
            return -4;
        else if(i==4 || i==5)
            return -3;
        else if(i==6 || i==7)
            return -2;
        else if(i==8 || i==9)
            return -1;
        else if(i==10 || i==11)
            return 0;
        else if(i==12 || i==13)
            return 1;
        else if(i==14 || i==15)
            return 2;
        else if(i==16 || i==17)
            return 3;
        else if(i==18 || i==19)
            return 4;
        else if(i==20 || i==21)
            return 5;
        else if(i==22 || i==23)
            return 6;
        else if(i==24 || i==25)
            return 7;
        else if(i==26 || i==27)
            return 8;
        else if(i==28 || i==29)
            return 9;
        else if(i>=30)
            return 10;
        else
            return -5;
    }


    public static List<String> getSkillsList(int force, int intelligence, int dexterity, int bodyType, int wisdom, int charisma){
        List<String> list = new ArrayList<>();

        list.add("Сил |" + (getMods(force) >= 0 ? "+"+getMods(force) : getMods(force)) + "|");
        list.add("Инт |" + (getMods(intelligence) >= 0 ? "+"+getMods(intelligence) : getMods(intelligence)) + "|");
        list.add("Лов |" + (getMods(dexterity) >= 0 ? "+"+getMods(dexterity) : getMods(dexterity)) + "|");
        list.add("Тел |" + (getMods(bodyType) >= 0 ? "+"+getMods(bodyType) : getMods(bodyType)) + "|");
        list.add("Мдр |" + (getMods(wisdom) >= 0 ? "+"+getMods(wisdom) : getMods(wisdom)) + "|");
        list.add("Хар |" + (getMods(charisma) >= 0 ? "+"+getMods(charisma) : getMods(charisma)) + "|");

        return list;
    }


    public static String rdm(int min, int max){
        return ""+((int)(Math.random()*(max - min) + min));
    }

    // Меню выбора расы
    private static boolean isInitedRaceMenu = false;
    static PopupMenu racePopupMenu;
    private static void raceMenu(Context context, View view, CharacterList cl){
        if(!isInitedRaceMenu){
            racePopupMenu = new PopupMenu(context, view);
            racePopupMenu.inflate(R.menu.races_menu);
            isInitedRaceMenu = true;
        }

        racePopupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                if(!item.isChecked()){

                    for(int j=0; j<racePopupMenu.getMenu().size(); j++){
                        racePopupMenu.getMenu().getItem(j).setEnabled(false);
                    }
                    item.setEnabled(true);
                    item.setChecked(!item.isChecked());

                    switch(item.getTitle().toString()){
                        case "Дварф":
                            cl.setRasFeatures("Тёмное зрение. Привыкнув к жизни под землёй, " +
                                    "вы обладаете превосходным зрением в темноте " +
                                    "и при тусклом освещении. На расстоянии в " +
                                    "60 футов вы при тусклом освещении можете видеть " +
                                    "так, как будто это яркое освещение, и в темноте " +
                                    "так, как будто это тусклое освещение. В темноте " +
                                    "вы не можете различать цвета, только оттенки серого.\n" +
                                    "Дварфская устойчивость. Вы совершаете с преимуществом " +
                                    "спасброски от яда, и вы получаете сопротивление " +
                                    "к урону ядом (объясняется в главе 9).\n" +
                                    "Дварфская боевая тренировка. Вы владеете боевым " +
                                    "топором, ручным топором, лёгким и боевым молотами.\n" +
                                    "Владение инструментами. Вы владеете ремесленными " +
                                    "инструментами на ваш выбор: инструменты " +
                                    "кузнеца, пивовара или каменщика.\n" +
                                    "Знание камня. Если вы совершаете проверку " +
                                    "Интеллекта (История), связанную с происхождением " +
                                    "работы по камню, вы считаетесь владеющим " +
                                    "навыком История, и добавляете к проверке удвоенный " +
                                    "бонус мастерства вместо обычного.");
                            cl.setSpeed(25);
                            break;
                        case "Эльф":
                            cl.setRasFeatures("Тёмное зрение. Привыкнув к сумраку леса и\n" +
                                    "ночному небу, вы обладаете превосходным зрением\n" +
                                    "в темноте и при тусклом освещении. На расстоя-\n" +
                                    "нии в 60 футов вы при тусклом освещении можете\n" +
                                    "видеть так, как будто это яркое освещение, и в\n" +
                                    "темноте так, как будто это тусклое освещение. В\n" +
                                    "темноте вы не можете различать цвета, только от-\n" +
                                    "тенки серого.\n" +
                                    "Обострённые чувства. Вы владеете навыком\n" +
                                    "Внимательность.\n" +
                                    "Наследие фей. Вы совершаете с преимуще-\n" +
                                    "ством спасброски от очарования, и вас невоз-\n" +
                                    "можно магически усыпить.\n" +
                                    "Транс. Эльфы не спят. Вместо этого они погру-\n" +
                                    "жаются в глубокую медитацию, находясь в полу-\n" +
                                    "бессознательном состоянии до 4 часов в сутки\n" +
                                    "(обычно такую медитацию называют трансом). Во\n" +
                                    "время этой медитации вы можете грезить о раз-\n" +
                                    "ных вещах. Некоторые из этих грёз являются мен-\n" +
                                    "тальными упражнениями, выработанными за годы\n" +
                                    "тренировок. После такого отдыха вы получаете все\n" +
                                    "преимущества, которые получает человек после 8\n" +
                                    "часов сна.");
                            cl.setSpeed(30);
                            break;
                        case "Полурослик":
                            cl.setRasFeatures("Везучий. Если при броске атаки, проверке ха-\n" +
                                    "рактеристики или спасброске у вас выпало «1», вы\n" +
                                    "можете перебросить кость, и должны использо-\n" +
                                    "вать новый результат.\n" +
                                    "Храбрый. Вы совершаете с преимуществом\n" +
                                    "спасброски от испуга.\n" +
                                    "Проворство полуросликов. Вы можете прохо-\n" +
                                    "дить сквозь пространство, занятое существами,\n" +
                                    "чей размер больше вашего.");
                            cl.setSpeed(25);
                            break;
                        case "Человек":
                            cl.setRasFeatures("");
                            cl.setSpeed(30);
                            break;
                        case "Драконорождённый":
                            cl.setRasFeatures("Оружие дыхания. Вы можете действием вы-\n" +
                                    "дохнуть разрушительную энергию. Ваше наследие\n" +
                                    "драконов определяет размер, форму и вид урона\n" +
                                    "вашего выдоха.\n" +
                                    "Когда вы используете оружие дыхания, все су-\n" +
                                    "щества в зоне выдоха должны совершить спасбро-\n" +
                                    "сок, вид которого определяется вашим наследием.\n" +
                                    "Сложность этого спасброска равна 8 + ваш моди-\n" +
                                    "фикатор Телосложения + ваш бонус мастерства.\n" +
                                    "Существа получают урона 2к6 в случае провален-\n" +
                                    "ного спасброска, или половину этого урона, если\n" +
                                    "спасбросок был успешен. Урон повышается до 3к6\n" +
                                    "на 6 уровне, до 4к6 на 11, и до 5к6 на 16 уровне.\n" +
                                    "После использования оружия дыхания вы не\n" +
                                    "можете использовать его повторно, пока не завер-\n" +
                                    "шите короткий либо продолжительный отдых.\n" +
                                    "Сопротивление урону. Вы получаете сопротив-\n" +
                                    "ление к урону того вида, который указан в вашем\n" +
                                    "наследии драконов.");
                            cl.setSpeed(30);
                            break;
                        case "Гном":
                            cl.setRasFeatures("Тёмное зрение. Привыкнув к жизни под землёй,\n" +
                                    "вы обладаете превосходным зрением в темноте и\n" +
                                    "при тусклом освещении. На расстоянии в 60 футов\n" +
                                    "вы при тусклом освещении можете видеть так, как\n" +
                                    "будто это яркое освещение, и в темноте так, как\n" +
                                    "будто это тусклое освещение. В темноте вы не мо-\n" +
                                    "жете различать цвета, только оттенки серого.\n" +
                                    "Гномья хитрость. Вы совершаете с преимуще-\n" +
                                    "ством спасброски Интеллекта, Мудрости и Ха-\n" +
                                    "ризмы против магии.\n" +
                                    "Языки. Вы можете говорить, читать и писать\n" +
                                    "на Общем и Гномьем языках. Гномий язык, ис-\n" +
                                    "пользующий дварфский алфавит, хорошо известен\n" +
                                    "благодаря техническим трактатам и каталогам");
                            cl.setSpeed(25);
                            break;
                        case "Полуэльф":
                            cl.setRasFeatures("Тёмное зрение. Благодаря вашей эльфийской\n" +
                                    "крови, вы обладаете превосходным зрением в тем-\n" +
                                    "ноте и при тусклом освещении. На расстоянии в\n" +
                                    "60 футов вы при тусклом освещении можете ви-\n" +
                                    "деть так, как будто это яркое освещение, и в тем-\n" +
                                    "ноте так, как будто это тусклое освещение. В тем-\n" +
                                    "ноте вы не можете различать цвета, только от-\n" +
                                    "тенки серого.\n" +
                                    "Наследие фей. Вы совершаете с преимуще-\n" +
                                    "ством спасброски от очарования, и вас невоз-\n" +
                                    "можно магически усыпить.");
                            cl.setSpeed(30);
                            break;
                        case "Полуорк":
                            cl.setRasFeatures("Тёмное зрение. Благодаря орочьей крови, вы\n" +
                                    "обладаете превосходным зрением в темноте и при\n" +
                                    "тусклом освещении. На расстоянии в 60 футов вы\n" +
                                    "при тусклом освещении можете видеть так, как\n" +
                                    "будто это яркое освещение, и в темноте так, как\n" +
                                    "будто это тусклое освещение. В темноте вы не мо-\n" +
                                    "жете различать цвета, только оттенки серого.\n" +
                                    "Угрожающий вид. Вы владеете навыком Запу-\n" +
                                    "гивание.\n" +
                                    "Непоколебимая стойкость. Если ваши хиты\n" +
                                    "опустились до нуля, но вы при этом не убиты,\n" +
                                    "ваши хиты вместо этого опускаются до 1. Вы не\n" +
                                    "можете использовать эту способность снова, пока\n" +
                                    "не завершите длительный отдых.\n" +
                                    "Свирепые атаки. Если вы совершили критиче-\n" +
                                    "ское попадание рукопашной атакой оружием, вы\n" +
                                    "можете добавить к урону ещё одну кость урона\n" +
                                    "оружия.");
                            cl.setSpeed(30);
                            break;
                        case "Тифлинг":
                            cl.setRasFeatures("Тёмное зрение. Благодаря вашему дьяволь-\n" +
                                    "скому наследию, вы отлично видите при тусклом\n" +
                                    "свете и в темноте. На расстоянии в 60 футов вы\n" +
                                    "при тусклом освещении можете видеть так, как\n" +
                                    "будто это яркое освещение, и в темноте так, как\n" +
                                    "будто это тусклое освещение. В темноте вы не мо-\n" +
                                    "жете различать цвета, только оттенки серого.\n" +
                                    "Адское сопротивление. Вы получаете сопротив-\n" +
                                    "ление к урону огнём.\n" +
                                    "Дьявольское наследие. Вы знаете заклинание\n" +
                                    "чудотворство. При достижении 3 уровня вы мо-\n" +
                                    "жете один раз в день активировать адское воз-\n" +
                                    "мездие как заклинание 2 уровня. При достижении\n" +
                                    "5 уровня вы также можете один раз в день акти-\n" +
                                    "вировать заклинание тьма. «Раз в день» означает,\n" +
                                    "что вы должны окончить продолжительный от-\n" +
                                    "дых, прежде чем сможете наложить это заклина-\n" +
                                    "ние ещё раз посредством данного умения. Базовой\n" +
                                    "характеристикой для этих заклинаний является\n" +
                                    "Харизма.");
                            cl.setSpeed(30);
                            break;
                    }

                    Toast.makeText(context, "Вы выбрали расу: "+item.getTitle().toString(), Toast.LENGTH_SHORT).show();
                    cl.setRace(item.getTitle().toString());
                } else{

                    for(int i=0; i<racePopupMenu.getMenu().size(); i++)
                        racePopupMenu.getMenu().getItem(i).setEnabled(true);
                    item.setChecked(false);
                    cl.setRace(null);
                    cl.setRasFeatures(null);
                    cl.setSpeed(0);
                }

                return true;
            }
        });

        racePopupMenu.show();
    }


    // Меню выбора класса
    private static boolean isInitedClassMenu = false;
    static PopupMenu classPopupMenu;
    private static void classMenu(Context context, View view, CharacterList cl){
        if(!isInitedClassMenu){
            classPopupMenu = new PopupMenu(context, view);
            classPopupMenu.inflate(R.menu.clases_menu);
            isInitedClassMenu = true;
        }

        classPopupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                if(!item.isChecked()){

                    for(int j=0; j<classPopupMenu.getMenu().size(); j++){
                        classPopupMenu.getMenu().getItem(j).setEnabled(false);
                    }
                    item.setEnabled(true);
                    item.setChecked(!item.isChecked());

                    switch(item.getTitle().toString()){
                        case "Бард":
                            cl.setDiceHP("1к8");
                            cl.setDexteritySavePossession(true);
                            cl.setCharismaSavePossession(true);
                            cl.setNumberOfFeaturesAvailable(3);
                            cl.setMaxHP(8 + cl.getBodyTypeMod());
                            break;
                        case "Варвар":
                            cl.setDiceHP("1к12");
                            cl.setForceSavePossession(true);
                            cl.setBodyTypeSavePossession(true);
                            cl.setNumberOfFeaturesAvailable(2);
                            cl.setMaxHP(12 + cl.getBodyTypeMod());
                            break;
                        case "Воин":
                            cl.setDiceHP("1к10");
                            cl.setForceSavePossession(true);
                            cl.setBodyTypeSavePossession(true);
                            cl.setNumberOfFeaturesAvailable(2);
                            cl.setMaxHP(10 + cl.getBodyTypeMod());
                            break;
                        case "Волшебник":
                            cl.setDiceHP("1к6");
                            cl.setIntellectSavePossession(true);
                            cl.setWisdomSavePossession(true);
                            cl.setNumberOfFeaturesAvailable(2);
                            cl.setMaxHP(6 + cl.getBodyTypeMod());
                            break;
                        case "Друид":
                            cl.setDiceHP("1к8");
                            cl.setIntellectSavePossession(true);
                            cl.setWisdomSavePossession(true);
                            cl.setNumberOfFeaturesAvailable(2);
                            cl.setMaxHP(8 + cl.getBodyTypeMod());
                            break;
                        case "Жрец":
                            cl.setDiceHP("1к8");
                            cl.setWisdomSavePossession(true);
                            cl.setCharismaSavePossession(true);
                            cl.setNumberOfFeaturesAvailable(2);
                            cl.setMaxHP(8 + cl.getBodyTypeMod());
                            break;
                        case "Колдун":
                            cl.setDiceHP("1к8");
                            cl.setWisdomSavePossession(true);
                            cl.setCharismaSavePossession(true);
                            cl.setNumberOfFeaturesAvailable(2);
                            cl.setMaxHP(8 + cl.getBodyTypeMod());
                            break;
                        case "Монах":
                            cl.setDiceHP("1к8");
                            cl.setForceSavePossession(true);
                            cl.setDexteritySavePossession(true);
                            cl.setNumberOfFeaturesAvailable(2);
                            cl.setMaxHP(8 + cl.getBodyTypeMod());
                            break;
                        case "Паладин":
                            cl.setDiceHP("1к10");
                            cl.setWisdomSavePossession(true);
                            cl.setCharismaSavePossession(true);
                            cl.setNumberOfFeaturesAvailable(2);
                            cl.setMaxHP(10 + cl.getBodyTypeMod());
                            break;
                        case "Плут":
                            cl.setDiceHP("1к8");
                            cl.setDexteritySavePossession(true);
                            cl.setIntellectSavePossession(true);
                            cl.setNumberOfFeaturesAvailable(4);
                            cl.setMaxHP(8 + cl.getBodyTypeMod());
                            break;
                        case "Следопыт":
                            cl.setDiceHP("1к10");
                            cl.setForceSavePossession(true);
                            cl.setDexteritySavePossession(true);
                            cl.setNumberOfFeaturesAvailable(3);
                            cl.setMaxHP(10 + cl.getBodyTypeMod());
                            break;
                        case "Чародей":
                            cl.setDiceHP("1к6");
                            cl.setBodyTypeSavePossession(true);
                            cl.setCharismaSavePossession(true);
                            cl.setNumberOfFeaturesAvailable(2);
                            cl.setMaxHP(6 + cl.getBodyTypeMod());
                            break;
                    }

                    Toast.makeText(context, "Вы выбрали класс: "+item.getTitle().toString(), Toast.LENGTH_SHORT).show();
                    cl.setClass(item.getTitle().toString());

                } else{

                    for(int i=0; i<classPopupMenu.getMenu().size(); i++)
                        classPopupMenu.getMenu().getItem(i).setEnabled(true);

//                    ForceSavePossession;     // Модификатор силы
//                    private boolean DexteritySavePossession; // Модификатор ловкости
//                    private boolean IntellectSavePossession; // Модификатор интеллекта
//                    private boolean EnduranceSavePossession; // Модификатор выносливости
//                    private boolean WisdomSavePossession;    // Модификатор мудрости
//                    private boolean CharismaSavePossession;


                    cl.setDiceHP(null);
                    cl.setForceSavePossession(false);
                    cl.setDexteritySavePossession(false);
                    cl.setIntellectSavePossession(false);
                    cl.setBodyTypeSavePossession(false);
                    cl.setWisdomSavePossession(false);
                    cl.setCharismaSavePossession(false);
                    cl.setNumberOfFeaturesAvailable(18);
                    cl.setMaxHP(0);

                    item.setChecked(false);
                    cl.setClass(null);
                }

                return true;
            }
        });

        classPopupMenu.show();
    }


    // Меню выбора происхождения
    private static boolean isInitedBackstoryMenu = false;
    static PopupMenu backstoryPopupMenu;
    private static void backstoryMenu(Context context, View view, CharacterList cl){
        if(!isInitedBackstoryMenu){
            backstoryPopupMenu = new PopupMenu(context, view);
            backstoryPopupMenu.inflate(R.menu.backstorys_menu);
            isInitedBackstoryMenu = true;
        }

        backstoryPopupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                if(item.isCheckable()){
                    if(!item.isChecked()){

                        for(int j=0; j<backstoryPopupMenu.getMenu().size(); j++){
                            if(!backstoryPopupMenu.getMenu().getItem(j).hasSubMenu())
                                backstoryPopupMenu.getMenu().getItem(j).setEnabled(false);
                            else
                                for(int i=0; i<backstoryPopupMenu.getMenu().getItem(j).getSubMenu().size(); i++)
                                    backstoryPopupMenu.getMenu().getItem(j).getSubMenu().getItem(i).setEnabled(false);
                        }
                        item.setEnabled(true);
                        item.setChecked(!item.isChecked());

                        Toast.makeText(context, "Вы выбрали предысторию: "+item.getTitle().toString(), Toast.LENGTH_SHORT).show();
                        cl.setOrigin(item.getTitle().toString());
                    } else{

                        for(int i=0; i<backstoryPopupMenu.getMenu().size(); i++)
                            if(!backstoryPopupMenu.getMenu().getItem(i).hasSubMenu())
                                backstoryPopupMenu.getMenu().getItem(i).setEnabled(true);
                            else
                                for(int j=0; j<backstoryPopupMenu.getMenu().getItem(i).getSubMenu().size(); j++)
                                    backstoryPopupMenu.getMenu().getItem(i).getSubMenu().getItem(j).setEnabled(true);
                        item.setChecked(false);
                        cl.setOrigin(null);
                    }
                }

                return true;
            }
        });

        backstoryPopupMenu.show();
    }


    // Меню выбора оружия
    private static boolean isInitedPopmenu = false;
    static PopupMenu popupMenu;
    private static void weaponMenu(Context context, View view, CharacterList cl){
        if(!isInitedPopmenu){
            popupMenu = new PopupMenu(context, view);
            popupMenu.inflate(R.menu.weapon_menu);
            isInitedPopmenu = true;
        }

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                CharacterList.Weapon weapon;

                if(!item.isChecked()){
                    switch(item.getItemId()){
                        case R.id.long_sword:
                            weapon = new CharacterList.Weapon("Длинный мечь", 0, "1к8", false);
                            cl.getWeapons().add(0, weapon);
                            break;
                        case R.id.short_sword:
                            weapon = new CharacterList.Weapon("Короткий мечь", 0, "1к6", false);
                            cl.getWeapons().add(1, weapon);
                            break;
                        case R.id.gross_messer:
                            weapon = new CharacterList.Weapon("Гроссмессер", 0, "1к6", false);
                            cl.getWeapons().add(2, weapon);
                            break;
                        case R.id.knife:
                            weapon = new CharacterList.Weapon("Нож", 0, "1к4", false);
                            cl.getWeapons().add(3, weapon);
                            break;
                        case R.id.two_handed_ax:
                            weapon = new CharacterList.Weapon("Двуручный топор", 0, "1к8", false);
                            cl.getWeapons().add(4, weapon);
                            break;
                        case R.id.one_handed_ax:
                            weapon = new CharacterList.Weapon("Одноручный топор", 0, "1к6", false);
                            cl.getWeapons().add(5, weapon);
                            break;
                        case R.id.small_ax:
                            weapon = new CharacterList.Weapon("Малый топор", 0, "1к6", false);
                            cl.getWeapons().add(6, weapon);
                            break;
                        case R.id.club:
                            weapon = new CharacterList.Weapon("Дубинка", 0, "1к4", false);
                            cl.getWeapons().add(7, weapon);
                            break;
                        case R.id.mace:
                            weapon = new CharacterList.Weapon("Булава", 0, "1к6", false);
                            cl.getWeapons().add(8, weapon);
                            break;
                        case R.id.improvised:
                            weapon = new CharacterList.Weapon("Импровизированное оружие", 0, "1к6", false);
                            cl.getWeapons().add(9, weapon);
                            break;
                        case R.id.spear:
                            weapon = new CharacterList.Weapon("Копьё", 0, "1к6", false);
                            cl.getWeapons().add(10, weapon);
                            break;
                        case R.id.halberd:
                            weapon = new CharacterList.Weapon("Алебарда", 0, "1к10", false);
                            cl.getWeapons().add(11, weapon);
                            break;
                        case R.id.staff:
                            weapon = new CharacterList.Weapon("Посох", 0, "1к6", false);
                            cl.getWeapons().add(12, weapon);
                            break;
                        case R.id.short_bow:
                            weapon = new CharacterList.Weapon("Короткий лук", 0, "1к6", false);
                            cl.getWeapons().add(13, weapon);
                            break;
                        case R.id.long_bow:
                            weapon = new CharacterList.Weapon("Длинный лук", 0, "1к8", false);
                            cl.getWeapons().add(14, weapon);
                            break;
                        case R.id.crossbow:
                            weapon = new CharacterList.Weapon("Арбалет", 0, "1к10", false);
                            cl.getWeapons().add(15, weapon);
                            break;
                        case R.id.dart:
                            weapon = new CharacterList.Weapon("Дротик", 0, "1к4", false);
                            cl.getWeapons().add(16, weapon);
                            break;
                    }
                } else{
                    switch(item.getItemId()){
                        case R.id.long_sword:
                            cl.getWeapons().add(0, null);
                            break;
                        case R.id.short_sword:
                            cl.getWeapons().add(1, null);
                            break;
                        case R.id.gross_messer:
                            cl.getWeapons().add(2, null);
                            break;
                        case R.id.knife:
                            cl.getWeapons().add(3, null);
                            break;
                        case R.id.two_handed_ax:
                            cl.getWeapons().add(4, null);
                            break;
                        case R.id.one_handed_ax:
                            cl.getWeapons().add(5, null);
                            break;
                        case R.id.small_ax:
                            cl.getWeapons().add(6, null);
                            break;
                        case R.id.club:
                            cl.getWeapons().add(7, null);
                            break;
                        case R.id.mace:
                            cl.getWeapons().add(8, null);
                            break;
                        case R.id.improvised:
                            cl.getWeapons().add(9, null);
                            break;
                        case R.id.spear:
                            cl.getWeapons().add(10, null);
                            break;
                        case R.id.halberd:
                            cl.getWeapons().add(11, null);
                            break;
                        case R.id.staff:
                            cl.getWeapons().add(12, null);
                            break;
                        case R.id.short_bow:
                            cl.getWeapons().add(13, null);
                            break;
                        case R.id.long_bow:
                            cl.getWeapons().add(14, null);
                            break;
                        case R.id.crossbow:
                            cl.getWeapons().add(15, null);
                            break;
                        case R.id.dart:
                            cl.getWeapons().add(16, null);
                            break;
                    }
                }

                Toast.makeText(context, "Вы выбрали: "+item.getTitle().toString(), Toast.LENGTH_SHORT).show();
                item.setChecked(!item.isChecked());

                return true;
            }
        });

//        popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
//            @Override
//            public void onDismiss(PopupMenu menu) {
//                //Toast.makeText(getContext(), "onDismiss", Toast.LENGTH_SHORT).show();
//            }
//        });

        popupMenu.show();
    }


    // Меню выбора доспехов
    private static boolean isInitedArmorMenu = false;
    static PopupMenu armorPopupMenu;
    private static void armorMenu(Context context, View view, CharacterList cl){
        if(!isInitedArmorMenu){
            armorPopupMenu = new PopupMenu(context, view);
            armorPopupMenu.inflate(R.menu.armor_menu);
            isInitedArmorMenu = true;
        }

        armorPopupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                if(item.isCheckable()){
                    if(!item.isChecked()){

                        for(int j=0; j<armorPopupMenu.getMenu().size(); j++){
                            if(!armorPopupMenu.getMenu().getItem(j).hasSubMenu())
                                armorPopupMenu.getMenu().getItem(j).setEnabled(false);
                            else
                                for(int i=0; i<armorPopupMenu.getMenu().getItem(j).getSubMenu().size(); i++)
                                    armorPopupMenu.getMenu().getItem(j).getSubMenu().getItem(i).setEnabled(false);
                        }
                        item.setEnabled(true);
                        item.setChecked(!item.isChecked());
                        Toast.makeText(context, "Вы выбрали: "+item.getTitle().toString(), Toast.LENGTH_SHORT).show();

                        cl.setArmorName(item.getTitle().toString());

                    } else{

                        for(int i=0; i<armorPopupMenu.getMenu().size(); i++)
                            if(!armorPopupMenu.getMenu().getItem(i).hasSubMenu())
                                armorPopupMenu.getMenu().getItem(i).setEnabled(true);
                            else
                                for(int j=0; j<armorPopupMenu.getMenu().getItem(i).getSubMenu().size(); j++)
                                    armorPopupMenu.getMenu().getItem(i).getSubMenu().getItem(j).setEnabled(true);
                        item.setChecked(false);

                        cl.setArmorName(null);
                        cl.setArmorClass(0);
                    }
                }

                return true;
            }
        });

        armorPopupMenu.show();
    }


    // Меню выбора языков
    private static boolean isInitedLangMenu = false;
    static PopupMenu langPopupMenu;
    private static void langMenu(Context context, View view, CharacterList cl){
        if(!isInitedLangMenu){
            langPopupMenu = new PopupMenu(context, view);
            langPopupMenu.inflate(R.menu.langs_menu);
            isInitedLangMenu = true;
        }

        langPopupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                if(!item.isChecked()){
                    switch(item.getItemId()){
                        case R.id.ogr_lang:
                            cl.getLanguageProficiency().add(0, "Язык великанов");
                            break;
                        case R.id.gnime_lang:
                            cl.getLanguageProficiency().add(1, "Язык гномов");
                            break;
                        case R.id.goblin_lang:
                            cl.getLanguageProficiency().add(2, "Язык гоблинов");
                            break;
                        case R.id.dwarf_lang:
                            cl.getLanguageProficiency().add(3, "Язык дварфов");
                            break;
                        case R.id.all_lang:
                            cl.getLanguageProficiency().add(4, "Общий язык");
                            break;
                        case R.id.orc_lang:
                            cl.getLanguageProficiency().add(5, "Язык орков");
                            break;
                        case R.id.halfling_lang:
                            cl.getLanguageProficiency().add(6, "Язык полуросликов");
                            break;
                        case R.id.elf_lang:
                            cl.getLanguageProficiency().add(7, "Язык эльфов");
                            break;
                        case R.id.hell_lang:
                            cl.getLanguageProficiency().add(8, "Язык безны");
                            break;
                        case R.id.dragon_lang:
                            cl.getLanguageProficiency().add(9, "Язык драконов");
                            break;
                    }
                } else{
                    switch(item.getItemId()){
                        case R.id.ogr_lang:
                            cl.getLanguageProficiency().add(0, null);
                            break;
                        case R.id.gnime_lang:
                            cl.getLanguageProficiency().add(1, null);
                            break;
                        case R.id.goblin_lang:
                            cl.getLanguageProficiency().add(2, null);
                            break;
                        case R.id.dwarf_lang:
                            cl.getLanguageProficiency().add(3, null);
                            break;
                        case R.id.all_lang:
                            cl.getLanguageProficiency().add(4, null);
                            break;
                        case R.id.orc_lang:
                            cl.getLanguageProficiency().add(5, null);
                            break;
                        case R.id.halfling_lang:
                            cl.getLanguageProficiency().add(6, null);
                            break;
                        case R.id.elf_lang:
                            cl.getLanguageProficiency().add(7, null);
                            break;
                        case R.id.hell_lang:
                            cl.getLanguageProficiency().add(8, null);
                            break;
                        case R.id.dragon_lang:
                            cl.getLanguageProficiency().add(9, null);
                            break;
                    }
                }

                Toast.makeText(context, "Вы выбрали: "+item.getTitle().toString(), Toast.LENGTH_SHORT).show();
                item.setChecked(!item.isChecked());

                // TODO: придумать, как удалять доавленные языки
//                cl.setLanguageProficiency(cl.getLanguageProficiency() + item.getTitle().toString() + "; ");

                return true;
            }
        });

        langPopupMenu.show();
    }
}
