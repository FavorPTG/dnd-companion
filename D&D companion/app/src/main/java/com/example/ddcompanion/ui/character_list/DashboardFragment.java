package com.example.ddcompanion.ui.character_list;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Debug;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.viewmodel.CreationExtras;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ddcompanion.CharacterList;
import com.example.ddcompanion.R;
import com.example.ddcompanion.Utilities;
import com.example.ddcompanion.adapters.ListAdapter;
import com.example.ddcompanion.databinding.FragmentDashboardBinding;

import java.nio.channels.AsynchronousChannelGroup;
import java.util.zip.Inflater;

public class DashboardFragment extends Fragment implements Runnable {

    private FragmentDashboardBinding binding;   // Содержит данные из fragment_dashboard.xml
    private ImageView menuButton;
    private CharacterList cl;
    Thread thread;
    private static UiTask asyncTask;

    private static boolean inited = false;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentDashboardBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        cl = new CharacterList();

        // testBlockStart 1.91
        ImageView compileCharacter = binding.editCharacterList;
        compileCharacter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utilities.createNewCharacterMenu(getContext(), cl).show();
            }
        });

        thread = new Thread(this);
        asyncTask = new UiTask();

        Log.println(Log.DEBUG, "MYTAG", "TEXT");
//        compileCharacter.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(cl.getRace() == null || cl.getRace().isEmpty()) {
//                    Toast.makeText(getContext(), "Не заполнено поле расы...", Toast.LENGTH_LONG).show();
//                    return;
//                }
//                else if(cl.getCharacterClass() == null || cl.getCharacterClass().isEmpty()) {
//                    Toast.makeText(getContext(), "Не заполнено поле класса...", Toast.LENGTH_LONG).show();
//                    return;
//                }
//                else if(cl.getOrigin() == null || cl.getOrigin().isEmpty()) {
//                    Toast.makeText(getContext(), "Не заполнено поле предыстории...", Toast.LENGTH_LONG).show();
//                    return;
//                }
////                else if(cl.getArmorClass() == 0) {
////                    Toast.makeText(getContext(), "Не не заполнено поле доспехов...", Toast.LENGTH_LONG).show();
////                    return;
////                }
//                else if(cl.getLanguageProficiency() == null || cl.getLanguageProficiency().isEmpty()){
//                    Toast.makeText(getContext(), "Не заполнено поле языков...", Toast.LENGTH_LONG).show();
//                    return;
//                }
//                else if(cl.getIdeology() == null || cl.getIdeology().isEmpty()) {
//                    Toast.makeText(getContext(), "Не заполнено поле идеологии...", Toast.LENGTH_LONG).show();
//                    return;
//                }
//
////    private int Force;     // Сила
////    private int Dexterity; // Ловкость
////    private int Intellect; // Интеллект
////    private int BodyType/endurance;  // Телосложение
////    private int Wisdom;    // Мудрость
////    private int Charisma;  // Харизма
//
//                cl.setForce(Integer.parseInt(binding.forceText.getText().toString()));
//                cl.setDexterity(Integer.parseInt(binding.dexterityText.getText().toString()));
//                cl.setIntellect(Integer.parseInt(binding.intelligeceText.getText().toString()));
//                cl.setBodyType(Integer.parseInt(binding.enduranceText.getText().toString()));
//                cl.setWisdom(Integer.parseInt(binding.wisdomText.getText().toString()));
//                cl.setCharisma(Integer.parseInt(binding.charismaText.getText().toString()));
//
//                cl.setCharacterName(binding.characterName.getText().toString());
//                cl.setUniqueFeatures(binding.skillsAndFeatures.getText().toString());
//
//                cl.compileCharacterList();
//
////                CharacterName;  // Имя персонажа
////                Race;           // Раса персонажа
////                Class;          // Класс персонажа
////                Origin;         // Происхождение персонажа
////                ideology;       // Мировоззрение персонажа
////
////                // Заполняемые игроком поля
////                LanguageProficiency; // Владение языками
////                UniqueFeatures;      // Уникальные особенности
////                BackStory;           // Предыстория персонажа
////
////                // Здоровье персонажа
////                MaxHP;        // Максимальные хиты
////                CurrentHP;    // Текущие хиты
////                TemporaryHP;  // Временные хиты
////                DiceHP;       // Кость хитов
////
////                // Модификаторы характеристик персонажа
////                ForceMod;     // Модификатор силы
////                DexterityMod; // Модификатор ловкости
////                IntellectMod; // Модификатор интеллекта
////                EnduranceMod; // Модификатор телосложения
////                WisdomMod;    // Модификатор мудрости
////                CharismaMod;  // Модификатор харизмы
////
////                // Спасброски
////                private int ForceSaveMod;     // Модификатор силы
////                private int DexteritySaveMod; // Модификатор ловкости
////                private int IntellectSaveMod; // Модификатор интеллекта
////                private int EnduranceSaveMod; // Модификатор выносливости
////                private int WisdomSaveMod;    // Модификатор мудрости
////                private int CharismaSaveMod;  // Модификатор харизмы
////
////                // Навыки
////                private int acrobatic;      // Акробатика
////                private int analise;        // Анализ
////                private int athletics;      // Атлетика
////                private int attentiveness;  // Внимательность
////                private int survival;       // Выживание
////                private int performance;    // Выступление
////                private int intimidation;   // Запугивание
////                private int history;        // История
////                private int sleightOfHand;  // Ловкость рук
////                private int magick;         // Магия
////                private int medicine;       // Медицина
////                private int deception;      // Обман
////                private int nature;         // Природа
////                private int insight;        // Проницательность
////                private int religion;       // Религия
////                private int secrecy;        // Скрытность
////                private int belief;         // Убеждение
////                private int animalCare;     // Уход за животными
////
////                // Второстепенные пункты
////                private int Inspiration;        // Вдохновение
////                private int SkillBonus;         // Бонус мастерства
////                private int PassiveMindfulness; // Пассивная внимательность
////                private int Speed;              // Скорость перемещения в футах (одна клетка = 5 футов)
//
//                if(charsList.getAdapter() != null){
//                    for(int i=0; i<chars.length; i++){
//                        charsList.getChildAt(i).setEnabled(false);
//                    }
//                    cl.compileCharacterList();
//                    charsList.getChildAt(0).setEnabled(cl.isAcrobaticPossession());
//                    charsList.getChildAt(1).setEnabled(cl.isAnalisePossession());
//                    charsList.getChildAt(2).setEnabled(cl.isAthleticsPossession());
//                    charsList.getChildAt(3).setEnabled(cl.isAttentivenessPossession());
//                    charsList.getChildAt(4).setEnabled(cl.isSurvivalPossession());
//                    charsList.getChildAt(5).setEnabled(cl.isPerformancePossession());
//                    charsList.getChildAt(6).setEnabled(cl.isIntimidationPossession());
//                    charsList.getChildAt(7).setEnabled(cl.isHistoryPossession());
//                    charsList.getChildAt(8).setEnabled(cl.isSleightOfHandPossession());
//                    charsList.getChildAt(9).setEnabled(cl.isMagickPossession());
//                    charsList.getChildAt(10).setEnabled(cl.isMedicinePossession());
//                    charsList.getChildAt(11).setEnabled(cl.isDeceptionPossession());
//                    charsList.getChildAt(12).setEnabled(cl.isNaturePossession());
//                    charsList.getChildAt(13).setEnabled(cl.isInsightPossession());
//                    charsList.getChildAt(14).setEnabled(cl.isReligionPossession());
//                    charsList.getChildAt(15).setEnabled(cl.isSecrecyPossession());
//                    charsList.getChildAt(16).setEnabled(cl.isBeliefPossession());
//                    charsList.getChildAt(17).setEnabled(cl.isAnimalCarePossession());
//                }
//
//                Utilities.createDialogMenu(getContext(), "Класс листа персонажа",
//                        "Имя персонажа: "+cl.getCharacterName()+"\n"+
//                        "Раса персонажа: "+cl.getRace()+"\n"+
//                        "Сласс персонажа: "+cl.getCharacterClass()+"\n"+
//                        "Происхождение персонажа: "+cl.getOrigin()+"\n"+
//                        "Мировоззрение персонажа: "+cl.getIdeology()+"\n"+
//                        "Владение языками: "+cl.getLanguageProficiency()+"\n"+
//                        "Уникальные особенности: "+cl.getUniqueFeatures()+"\n"+
//                        "Максимальное здоровье: "+cl.getMaxHP()+"\n"+
//                        "Текрщее здоровье: "+cl.getCurrentHP()+"\n"+
//
//                        "\nМодификатор силы:"+cl.getForceMod()+"\n"+
//                        "Модификатор ловкости: "+cl.getDexterityMod()+"\n"+
//                        "Модификатор интелекта: "+cl.getIntellectMod()+"\n"+
//                        "Модификатор телосложение: "+cl.getEnduranceMod()+"\n"+
//                        "Модификатор мудрости: "+cl.getWisdomMod()+"\n"+
//                        "Модификатор харизмы: "+cl.getCharismaMod()+"\n"+
//
//                        "\nМодификатор спасброска силы: "+cl.getForceSaveMod()+"\n"+
//                        "Модификатор спасброска ловкости: "+cl.getDexteritySaveMod()+"\n"+
//                        "Модификатор спасброска интеллекта: "+cl.getIntellectSaveMod()+"\n"+
//                        "Модификатор спасброска телосложения: "+cl.getEnduranceSaveMod()+"\n"+
//                        "Модификатор спасброска мудрости: "+cl.getWisdomSaveMod()+"\n"+
//                        "Модификатор спасброска харизмы: "+cl.getCharismaSaveMod()+"\n"+
//
//                        "\nЗначение акробатики: "+cl.getAcrobatic()+"\n"+
//                        "Значение анализа: "+cl.getAnalise()+"\n"+
//                        "Значение атлетики: "+cl.getAthletics()+"\n"+
//                        "Значение внимательности: "+cl.getAttentiveness()+"\n"+
//                        "Значение выживания: "+cl.getSurvival()+"\n"+
//                        "Значение выступления: "+cl.getPerformance()+"\n"+
//                        "Значение запугивания: "+cl.getIntimidation()+"\n"+
//                        "Значение истории: "+cl.getHistory()+"\n"+
//                        "Значение ловкости рук: "+cl.getSleightOfHand()+"\n"+
//                        "Значение магии: "+cl.getMagick()+"\n"+
//                        "Значение медицыны: "+cl.getMedicine()+"\n"+
//                        "Значение обмана: "+cl.getDeception()+"\n"+
//                        "Значение природы: "+cl.getNature()+"\n"+
//                        "Значение проницательности: "+cl.getInsight()+"\n"+
//                        "Значение религии: "+cl.getReligion()+"\n"+
//                        "Значение скрытности: "+cl.getSecrecy()+"\n"+
//                        "Значение убеждения: "+cl.getBelief()+"\n"+
//                        "Значение ухоода за животными: "+cl.getAnimalCare()+"\n"+
//                        "\nЗначение бонуса мастерства: "+cl.getSkillBonus(), null, "Закрыть");
//
//
//
////                Utilities.createFloatMenu(getContext(), "", R.layout.compiled_character_list).show();
//                Utilities.createNewCharacterMenu(getContext(), cl).show();
//            }
//        });
        Log.println(Log.DEBUG, "MYTAG", "TEXT");


//        TODO: Альтернатива всплывающему меню на будущее (ListView)
//        menuIsActive = false;
//        ListView listView = binding.listView;
//        final String[] items = {getResources().getString(R.string.dwarf_name),
//                getResources().getString(R.string.elf_name), getResources().getString(R.string.halfling_name),
//                getResources().getString(R.string.human_name), getResources().getString(R.string.dragonborn_name),
//                getResources().getString(R.string.gnome_name), getResources().getString(R.string.half_elf_name),
//                getResources().getString(R.string.half_orc_name), getResources().getString(R.string.tifling_name)};
//        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_multiple_choice, items);
//        listView.setAdapter(arrayAdapter);
//        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
//                switch(position){
//                    case 0:
//                        Utilities.createDialogMenu(getContext(), getResources().getString(R.string.dwarf_name),
//                                getResources().getString(R.string.dwarf_desc), null, "Назад").show();
//                        break;
//                    case 1:
//                        Utilities.createDialogMenu(getContext(), getResources().getString(R.string.elf_name),
//                                getResources().getString(R.string.elf_desc), null, "Назад").show();
//                        break;
//                    case 2:
//                        Utilities.createDialogMenu(getContext(), getResources().getString(R.string.halfling_name),
//                                getResources().getString(R.string.hlafling_desc), null, "Назад").show();
//                        break;
//                    case 3:
//                        Utilities.createDialogMenu(getContext(), getResources().getString(R.string.human_name),
//                                getResources().getString(R.string.human_desc), null, "Назад").show();
//                        break;
//                    case 4:
//                        Utilities.createDialogMenu(getContext(), getResources().getString(R.string.dragonborn_name),
//                                getResources().getString(R.string.dragonborn_desc), null, "Назад").show();
//                        break;
//                    case 5:
//                        Utilities.createDialogMenu(getContext(), getResources().getString(R.string.gnome_name),
//                                getResources().getString(R.string.gnome_desc), null, "Назад").show();
//                        break;
//                    case 6:
//                        Utilities.createDialogMenu(getContext(), getResources().getString(R.string.half_elf_name),
//                                getResources().getString(R.string.half_elf_desc), null, "Назад").show();
//                        break;
//                    case 7:
//                        Utilities.createDialogMenu(getContext(), getResources().getString(R.string.half_orc_name),
//                                getResources().getString(R.string.half_orc_desc), null, "Назад").show();
//                        break;
//                    case 8:
//                        Utilities.createDialogMenu(getContext(), getResources().getString(R.string.tifling_name),
//                                getResources().getString(R.string.teifling_desc), null, "Назад").show();
//                        break;
//                }
//                return true;
//            }
//        });
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                CheckedTextView ctv = (CheckedTextView) view;
//                if(!menuIsActive){
//                    for(int i=0; i<items.length; i++){
//                        CheckedTextView v = (CheckedTextView) listView.getChildAt(i);
//                        v.setChecked(false);
//                        v.setEnabled(false);
//                    }
//                    ctv.setEnabled(true);
//                    ctv.setChecked(!ctv.isChecked());
//                    menuIsActive = true;
//                    oldPos = position;
//                } else if(position == oldPos){
//                    for(int j=0; j<items.length; j++){
//                        CheckedTextView ct = (CheckedTextView) listView.getChildAt(j);
//                        ct.setChecked(false);
//                        ct.setEnabled(true);
//                    }
//                    menuIsActive = false;
//                }
//            }
//        });
//         testBlockEnd 1.91


//         Установка слушателей для кубиков
        binding.throwD4.setOnClickListener(diceListener);
        binding.throwD6.setOnClickListener(diceListener);
        binding.throwD10.setOnClickListener(diceListener);
        binding.throwD12.setOnClickListener(diceListener);
        binding.throwD20.setOnClickListener(diceListener);
        binding.throwD100.setOnClickListener(diceListener);

        // Вызов боковых меню
        menuButton = binding.menuButton;
        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utilities.animate(binding.rightMenu);
            }
        });
        binding.SaLmenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utilities.animate(binding.SaLMenu);
            }
        });

//        thread.start();

        return root;
    }


    // Обработчик кнопок кубиков
    View.OnClickListener diceListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.throwD4:
                    binding.diceText.setText(Utilities.rdm(1, 5));
                    break;
                case R.id.throwD6:
                    binding.diceText.setText(Utilities.rdm(1, 7));
                    break;
                case R.id.throwD10:
                    binding.diceText.setText(Utilities.rdm(1, 11));
                    break;
                case R.id.throwD12:
                    binding.diceText.setText(Utilities.rdm(1, 13));
                    break;
                case R.id.throwD20:
                    binding.diceText.setText(Utilities.rdm(1, 21));
                    break;
                case R.id.throwD100:
                    binding.diceText.setText(Utilities.rdm(1, 101));
                    break;
                default:
                    binding.diceText.setText("Error");
            }
        }
    };



    public static void executeTesk(){
        asyncTask.execute();
    }


    public static boolean isinited() { return inited; }
    public static void setinited(boolean inited) { DashboardFragment.inited = inited; }




    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    // TODO: В отдельном потоке нужно отслеживать изменения в листе персонажа и если сработает триггер в классе листа - обновить данные
    @Override
    public void run() {
//        Log.println(Log.DEBUG, "DEBUG", "FINE)))");
        if(cl.isWantToUpdate()){
            Toast.makeText(getContext(), "Fine)))", Toast.LENGTH_SHORT).show();
            binding.nameText.setText(cl.getCharacterName());
            binding.classText.setText(cl.getCharacterClass());
            binding.rasText.setText(cl.getRace());
            binding.originText.setText(cl.getOrigin());
            binding.ideologyText.setText(cl.getIdeology());

            binding.forceChar.setText(cl.getForce());
            binding.dexterityChar.setText(cl.getDexterity());
            binding.bodyTypeChar.setText(cl.getBodyType());
            binding.intellectChar.setText(cl.getIntellect());
            binding.wisdomChar.setText(cl.getWisdom());
            binding.charismaChar.setText(cl.getCharisma());

            binding.forceMod.setText(cl.getForceMod());
            binding.dexterityMod.setText(cl.getDexterityMod());
            binding.bodyTypeMod.setText(cl.getBodyTypeMod());
            binding.intellectMod.setText(cl.getIntellectMod());
            binding.wisdomMod.setText(cl.getWisdomMod());
            binding.charismaMod.setText(cl.getCharismaMod());

            binding.forceSave.setText(cl.getForceSaveMod());
            binding.dexteritySave.setText(cl.getDexteritySaveMod());
            binding.bodyTypeSave.setText(cl.getEnduranceSaveMod());
            binding.intellectSave.setText(cl.getIntellectSaveMod());
            binding.wisdomSave.setText(cl.getWisdomSaveMod());
            binding.charismaSave.setText(cl.getCharismaSaveMod());

            binding.forceSave.setChecked(cl.isForceSavePossession());
            binding.dexteritySave.setChecked(cl.isDexteritySavePossession());
            binding.bodyTypeSave.setChecked(cl.isBodyTypeSavePossession());
            binding.intellectSave.setChecked(cl.isIntellectSavePossession());
            binding.wisdomSave.setChecked(cl.isWisdomSavePossession());
            binding.charismaSave.setChecked(cl.isCharismaSavePossession());

            binding.forceSave.setText(cl.getForceSaveMod() < 0 ? ""+cl.getForceSaveMod() : "+"+cl.getForceSaveMod());
            binding.dexteritySave.setText(cl.getDexteritySaveMod() < 0 ? ""+cl.getDexteritySaveMod() : "+"+cl.getDexteritySaveMod());
            binding.bodyTypeSave.setText(cl.getEnduranceSaveMod() < 0 ? ""+cl.getEnduranceSaveMod() : "+"+cl.getEnduranceSaveMod());
            binding.intellectSave.setText(cl.getIntellectSaveMod() < 0 ? ""+cl.getIntellectSaveMod() : "+"+cl.getIntellectSaveMod());
            binding.wisdomSave.setText(cl.getWisdomSaveMod() < 0 ? ""+cl.getWisdomSaveMod() : "+"+cl.getWisdomSaveMod());
            binding.charismaSave.setText(cl.getCharismaSaveMod() < 0 ? ""+cl.getCharismaSaveMod() : "+"+cl.getCharismaSaveMod());

            //характеристики
            binding.acrobaticMod.setChecked(cl.isAcrobaticPossession());
            binding.analiseMod.setChecked(cl.isAnalisePossession());
            binding.athleticsMod.setChecked(cl.isAthleticsPossession());
            binding.attentivenessMod.setChecked(cl.isAttentivenessPossession());
            binding.survivalMod.setChecked(cl.isSurvivalPossession());
            binding.performanceMod.setChecked(cl.isPerformancePossession());
            binding.intimidationMod.setChecked(cl.isIntimidationPossession());
            binding.historyMod.setChecked(cl.isHistoryPossession());
            binding.sleightOfHandMod.setChecked(cl.isSleightOfHandPossession());
            binding.magickMod.setChecked(cl.isMagickPossession());
            binding.medicineMod.setChecked(cl.isMedicinePossession());
            binding.deceptionMod.setChecked(cl.isDeceptionPossession());
            binding.natureMod.setChecked(cl.isNaturePossession());
            binding.insightMod.setChecked(cl.isInsightPossession());
            binding.religionMod.setChecked(cl.isReligionPossession());
            binding.secrecyMod.setChecked(cl.isSecrecyPossession());
            binding.beliefMod.setChecked(cl.isBeliefPossession());
            binding.animalCareMod.setChecked(cl.isAnimalCarePossession());

            binding.acrobaticMod.setText(cl.getAcrobatic());
            binding.analiseMod.setText(cl.getAnalise());
            binding.athleticsMod.setText(cl.getAthletics());
            binding.attentivenessMod.setText(cl.getAttentiveness());
            binding.survivalMod.setText(cl.getSurvival());
            binding.performanceMod.setText(cl.getPerformance());
            binding.intimidationMod.setText(cl.getIntimidation());
            binding.historyMod.setText(cl.getHistory());
            binding.sleightOfHandMod.setText(cl.getSleightOfHand());
            binding.magickMod.setText(cl.getMagick());
            binding.medicineMod.setText(cl.getMedicine());
            binding.deceptionMod.setText(cl.getDeception());
            binding.natureMod.setText(cl.getNature());
            binding.insightMod.setText(cl.getInsight());
            binding.religionMod.setText(cl.getReligion());
            binding.secrecyMod.setText(cl.getSecrecy());
            binding.beliefMod.setText(cl.getBelief());
            binding.animalCareMod.setText(cl.getAnimalCare());

            binding.passiveWisdom.setText(cl.getPassiveMindfulness());
            binding.skillBonus.setText(cl.getSkillBonus());

            //хиты

            binding.KDText.setText(cl.getArmorClass());
            binding.iniciativeText.setText(cl.getDexterityMod() < 0 ? ""+cl.getDexterityMod() : "+"+cl.getDexterityMod());
            //скорость

            for(int i=0; i<cl.getLanguageProficiency().size(); i++){
                if(cl.getLanguageProficiency().get(i) != null)
                    binding.langsText.setText(binding.langsText.getText() + "; " + cl.getLanguageProficiency().get(i));
            }

            binding.uniqueFeaturesText.setText("Расовые особенности + \n"+cl.getUniqueFeatures());

            TextView tv = binding.equipmentText;
            for(int i=0; i<cl.getWeapons().size(); i++){
                if(cl.getWeapons().get(i) != null)
                    tv.setText(tv.getText() +
                            cl.getWeapons().get(i).getWeaponName() + " - " + cl.getWeapons().get(i).getDamageDice() + " урона\n");
            }

            cl.setWantToUpdate(false);
        }
    }



    class UiTask extends AsyncTask<Void, Void, Void>{
        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }

//        @Override
//        protected void onProgressUpdate(Void... values) {
//            super.onProgressUpdate(values);
//            Toast.makeText(getContext(), "Пток создался)))", Toast.LENGTH_SHORT).show();
//            if(cl.isWantToUpdate()){
//            Toast.makeText(getContext(), "Fine)))", Toast.LENGTH_SHORT).show();
//                binding.nameText.setText(cl.getCharacterName());
//                binding.classText.setText(cl.getCharacterClass());
//                binding.rasText.setText(cl.getRace());
//                binding.originText.setText(cl.getOrigin());
//                binding.ideologyText.setText(cl.getIdeology());
//
//                binding.forceChar.setText(cl.getForce());
//                binding.dexterityChar.setText(cl.getDexterity());
//                binding.bodyTypeChar.setText(cl.getBodyType());
//                binding.intellectChar.setText(cl.getIntellect());
//                binding.wisdomChar.setText(cl.getWisdom());
//                binding.charismaChar.setText(cl.getCharisma());
//
//                binding.forceMod.setText(cl.getForceMod());
//                binding.dexterityMod.setText(cl.getDexterityMod());
//                binding.bodyTypeMod.setText(cl.getBodyTypeMod());
//                binding.intellectMod.setText(cl.getIntellectMod());
//                binding.wisdomMod.setText(cl.getWisdomMod());
//                binding.charismaMod.setText(cl.getCharismaMod());
//
//                binding.forceSave.setText(cl.getForceSaveMod());
//                binding.dexteritySave.setText(cl.getDexteritySaveMod());
//                binding.bodyTypeSave.setText(cl.getEnduranceSaveMod());
//                binding.intellectSave.setText(cl.getIntellectSaveMod());
//                binding.wisdomSave.setText(cl.getWisdomSaveMod());
//                binding.charismaSave.setText(cl.getCharismaSaveMod());
//
//                binding.forceSave.setChecked(cl.isForceSavePossession());
//                binding.dexteritySave.setChecked(cl.isDexteritySavePossession());
//                binding.bodyTypeSave.setChecked(cl.isBodyTypeSavePossession());
//                binding.intellectSave.setChecked(cl.isIntellectSavePossession());
//                binding.wisdomSave.setChecked(cl.isWisdomSavePossession());
//                binding.charismaSave.setChecked(cl.isCharismaSavePossession());
//
//                binding.forceSave.setText(cl.getForceSaveMod() < 0 ? ""+cl.getForceSaveMod() : "+"+cl.getForceSaveMod());
//                binding.dexteritySave.setText(cl.getDexteritySaveMod() < 0 ? ""+cl.getDexteritySaveMod() : "+"+cl.getDexteritySaveMod());
//                binding.bodyTypeSave.setText(cl.getEnduranceSaveMod() < 0 ? ""+cl.getEnduranceSaveMod() : "+"+cl.getEnduranceSaveMod());
//                binding.intellectSave.setText(cl.getIntellectSaveMod() < 0 ? ""+cl.getIntellectSaveMod() : "+"+cl.getIntellectSaveMod());
//                binding.wisdomSave.setText(cl.getWisdomSaveMod() < 0 ? ""+cl.getWisdomSaveMod() : "+"+cl.getWisdomSaveMod());
//                binding.charismaSave.setText(cl.getCharismaSaveMod() < 0 ? ""+cl.getCharismaSaveMod() : "+"+cl.getCharismaSaveMod());
//
//                //характеристики
//                binding.acrobaticMod.setChecked(cl.isAcrobaticPossession());
//                binding.analiseMod.setChecked(cl.isAnalisePossession());
//                binding.athleticsMod.setChecked(cl.isAthleticsPossession());
//                binding.attentivenessMod.setChecked(cl.isAttentivenessPossession());
//                binding.survivalMod.setChecked(cl.isSurvivalPossession());
//                binding.performanceMod.setChecked(cl.isPerformancePossession());
//                binding.intimidationMod.setChecked(cl.isIntimidationPossession());
//                binding.historyMod.setChecked(cl.isHistoryPossession());
//                binding.sleightOfHandMod.setChecked(cl.isSleightOfHandPossession());
//                binding.magickMod.setChecked(cl.isMagickPossession());
//                binding.medicineMod.setChecked(cl.isMedicinePossession());
//                binding.deceptionMod.setChecked(cl.isDeceptionPossession());
//                binding.natureMod.setChecked(cl.isNaturePossession());
//                binding.insightMod.setChecked(cl.isInsightPossession());
//                binding.religionMod.setChecked(cl.isReligionPossession());
//                binding.secrecyMod.setChecked(cl.isSecrecyPossession());
//                binding.beliefMod.setChecked(cl.isBeliefPossession());
//                binding.animalCareMod.setChecked(cl.isAnimalCarePossession());
//
//                binding.acrobaticMod.setText(cl.getAcrobatic());
//                binding.analiseMod.setText(cl.getAnalise());
//                binding.athleticsMod.setText(cl.getAthletics());
//                binding.attentivenessMod.setText(cl.getAttentiveness());
//                binding.survivalMod.setText(cl.getSurvival());
//                binding.performanceMod.setText(cl.getPerformance());
//                binding.intimidationMod.setText(cl.getIntimidation());
//                binding.historyMod.setText(cl.getHistory());
//                binding.sleightOfHandMod.setText(cl.getSleightOfHand());
//                binding.magickMod.setText(cl.getMagick());
//                binding.medicineMod.setText(cl.getMedicine());
//                binding.deceptionMod.setText(cl.getDeception());
//                binding.natureMod.setText(cl.getNature());
//                binding.insightMod.setText(cl.getInsight());
//                binding.religionMod.setText(cl.getReligion());
//                binding.secrecyMod.setText(cl.getSecrecy());
//                binding.beliefMod.setText(cl.getBelief());
//                binding.animalCareMod.setText(cl.getAnimalCare());
//
//                binding.passiveWisdom.setText(cl.getPassiveMindfulness());
//                binding.skillBonus.setText(cl.getSkillBonus());
//
//                //хиты
//
//                binding.KDText.setText(cl.getArmorClass());
//                binding.iniciativeText.setText(cl.getDexterityMod() < 0 ? ""+cl.getDexterityMod() : "+"+cl.getDexterityMod());
//                //скорость
//
//                for(int i=0; i<cl.getLanguageProficiency().size(); i++){
//                    if(cl.getLanguageProficiency().get(i) != null)
//                        binding.langsText.setText(binding.langsText.getText() + "; " + cl.getLanguageProficiency().get(i));
//                }
//
//                binding.uniqueFeaturesText.setText("Расовые особенности + \n"+cl.getUniqueFeatures());
//
//                TextView tv = binding.equipmentText;
//                for(int i=0; i<cl.getWeapons().size(); i++){
//                    if(cl.getWeapons().get(i) != null)
//                        tv.setText(tv.getText() +
//                                cl.getWeapons().get(i).getWeaponName() + " - " + cl.getWeapons().get(i).getDamageDice() + " урона\n");
//                }
//
//                cl.setWantToUpdate(false);
//            }
//        }

        @Override
        protected void onPostExecute(Void unused) {
            super.onPostExecute(unused);

            if(cl.isWantToUpdate()){
//                    Toast.makeText(getContext(), "Fine)))", Toast.LENGTH_SHORT).show();
                binding.nameText.setText(cl.getCharacterName());
                binding.classText.setText(cl.getCharacterClass());
                binding.rasText.setText(cl.getRace());
                binding.originText.setText(cl.getOrigin());
                binding.ideologyText.setText(cl.getIdeology());

                binding.forceChar.setText(""+cl.getForce());
                binding.dexterityChar.setText(""+cl.getDexterity());
                binding.bodyTypeChar.setText(""+cl.getBodyType());
                binding.intellectChar.setText(""+cl.getIntellect());
                binding.wisdomChar.setText(""+cl.getWisdom());
                binding.charismaChar.setText(""+cl.getCharisma());

                binding.forceMod.setText(""+cl.getForceMod());
                binding.dexterityMod.setText(""+cl.getDexterityMod());
                binding.bodyTypeMod.setText(""+cl.getBodyTypeMod());
                binding.intellectMod.setText(""+cl.getIntellectMod());
                binding.wisdomMod.setText(""+cl.getWisdomMod());
                binding.charismaMod.setText(""+cl.getCharismaMod());

                binding.forceSave.setText(""+cl.getForceSaveMod());
                binding.dexteritySave.setText(""+cl.getDexteritySaveMod());
                binding.bodyTypeSave.setText(""+cl.getEnduranceSaveMod());
                binding.intellectSave.setText(""+cl.getIntellectSaveMod());
                binding.wisdomSave.setText(""+cl.getWisdomSaveMod());
                binding.charismaSave.setText(""+cl.getCharismaSaveMod());

                binding.forceSave.setChecked(cl.isForceSavePossession());
                binding.dexteritySave.setChecked(cl.isDexteritySavePossession());
                binding.bodyTypeSave.setChecked(cl.isBodyTypeSavePossession());
                binding.intellectSave.setChecked(cl.isIntellectSavePossession());
                binding.wisdomSave.setChecked(cl.isWisdomSavePossession());
                binding.charismaSave.setChecked(cl.isCharismaSavePossession());

                binding.forceSave.setText(cl.getForceSaveMod() < 0 ? ""+cl.getForceSaveMod() : "+"+cl.getForceSaveMod());
                binding.dexteritySave.setText(cl.getDexteritySaveMod() < 0 ? ""+cl.getDexteritySaveMod() : "+"+cl.getDexteritySaveMod());
                binding.bodyTypeSave.setText(cl.getEnduranceSaveMod() < 0 ? ""+cl.getEnduranceSaveMod() : "+"+cl.getEnduranceSaveMod());
                binding.intellectSave.setText(cl.getIntellectSaveMod() < 0 ? ""+cl.getIntellectSaveMod() : "+"+cl.getIntellectSaveMod());
                binding.wisdomSave.setText(cl.getWisdomSaveMod() < 0 ? ""+cl.getWisdomSaveMod() : "+"+cl.getWisdomSaveMod());
                binding.charismaSave.setText(cl.getCharismaSaveMod() < 0 ? ""+cl.getCharismaSaveMod() : "+"+cl.getCharismaSaveMod());

                //характеристики
                binding.acrobaticMod.setChecked(cl.isAcrobaticPossession());
                binding.analiseMod.setChecked(cl.isAnalisePossession());
                binding.athleticsMod.setChecked(cl.isAthleticsPossession());
                binding.attentivenessMod.setChecked(cl.isAttentivenessPossession());
                binding.survivalMod.setChecked(cl.isSurvivalPossession());
                binding.performanceMod.setChecked(cl.isPerformancePossession());
                binding.intimidationMod.setChecked(cl.isIntimidationPossession());
                binding.historyMod.setChecked(cl.isHistoryPossession());
                binding.sleightOfHandMod.setChecked(cl.isSleightOfHandPossession());
                binding.magickMod.setChecked(cl.isMagickPossession());
                binding.medicineMod.setChecked(cl.isMedicinePossession());
                binding.deceptionMod.setChecked(cl.isDeceptionPossession());
                binding.natureMod.setChecked(cl.isNaturePossession());
                binding.insightMod.setChecked(cl.isInsightPossession());
                binding.religionMod.setChecked(cl.isReligionPossession());
                binding.secrecyMod.setChecked(cl.isSecrecyPossession());
                binding.beliefMod.setChecked(cl.isBeliefPossession());
                binding.animalCareMod.setChecked(cl.isAnimalCarePossession());

                binding.acrobaticMod.setText(""+cl.getAcrobatic());
                binding.analiseMod.setText(""+cl.getAnalise());
                binding.athleticsMod.setText(""+cl.getAthletics());
                binding.attentivenessMod.setText(""+cl.getAttentiveness());
                binding.survivalMod.setText(""+cl.getSurvival());
                binding.performanceMod.setText(""+cl.getPerformance());
                binding.intimidationMod.setText(""+cl.getIntimidation());
                binding.historyMod.setText(""+cl.getHistory());
                binding.sleightOfHandMod.setText(""+cl.getSleightOfHand());
                binding.magickMod.setText(""+cl.getMagick());
                binding.medicineMod.setText(""+cl.getMedicine());
                binding.deceptionMod.setText(""+cl.getDeception());
                binding.natureMod.setText(""+cl.getNature());
                binding.insightMod.setText(""+cl.getInsight());
                binding.religionMod.setText(""+cl.getReligion());
                binding.secrecyMod.setText(""+cl.getSecrecy());
                binding.beliefMod.setText(""+cl.getBelief());
                binding.animalCareMod.setText(""+cl.getAnimalCare());

                binding.passiveWisdom.setText(""+cl.getPassiveMindfulness());
                binding.skillBonus.setText(""+cl.getSkillBonus());

                binding.maxHPText.setText(""+cl.getMaxHP());
                binding.currentHPText.setText(""+cl.getCurrentHP());
                binding.diceHPText.setText(""+cl.getDiceHP());

                binding.KDText.setText(""+cl.getArmorClass());
                binding.iniciativeText.setText(cl.getDexterityMod() < 0 ? ""+cl.getDexterityMod() : "+"+cl.getDexterityMod());
                binding.speedText.setText(""+cl.getSpeed());

                for(int i=0; i<cl.getLanguageProficiency().size(); i++){
                    if(cl.getLanguageProficiency().get(i) != null)
                        binding.langsText.setText(binding.langsText.getText() + cl.getLanguageProficiency().get(i) + "; ");
                }

                binding.uniqueFeaturesText.setText(cl.getUniqueFeatures() + "\n" + cl.getRasFeatures());

                TextView tv = binding.equipmentText;
                for(int i=0; i<cl.getWeapons().size(); i++){
                    if(cl.getWeapons().get(i) != null)
                        tv.setText(tv.getText() +
                                cl.getWeapons().get(i).getWeaponName() + " - " + cl.getWeapons().get(i).getDamageDice() + " урона\n");
                }

                cl.setWantToUpdate(false);
            }
        }
    }
}