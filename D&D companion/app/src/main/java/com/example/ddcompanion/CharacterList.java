package com.example.ddcompanion;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class CharacterList {

    private boolean wantToUpdate = false;

    // Информация для заголовка листа
    private String CharacterName;  // Имя персонажа
    private int CharacterLevel;    // Уровень персонажа
    private String Race;           // Раса персонажа
    private String Class;          // Класс персонажа
    private String Origin;         // Происхождение персонажа
    private String ideology;       // Мировоззрение персонажа

    // Заполняемые игроком поля
    private List<String> LanguageProficiency; // Владение языками
    private String UniqueFeatures;            // Уникальные особенности
    private String RasFeatures;               // Расовые особенности
    private String BackStory;                 // Предыстория персонажа

    // Здоровье персонажа
    private int MaxHP;        // Максимальные хиты
    private int CurrentHP;    // Текущие хиты
    private int TemporaryHP;  // Временные хиты
    private String DiceHP;    // Кость хитов

    // Значения характеристик персонажа
    private int Force;     // Сила
    private int Dexterity; // Ловкость
    private int Intellect; // Интеллект
    private int BodyType;  // Телосложение
    private int Wisdom;    // Мудрость
    private int Charisma;  // Харизма

    // Модификаторы характеристик персонажа
    private int ForceMod;     // Модификатор силы
    private int DexterityMod; // Модификатор ловкости
    private int IntellectMod; // Модификатор интеллекта
    private int EnduranceMod; // Модификатор телосложения
    private int WisdomMod;    // Модификатор мудрости
    private int CharismaMod;  // Модификатор харизмы

    // Спасброски
    private int ForceSaveMod;     // Модификатор силы
    private int DexteritySaveMod; // Модификатор ловкости
    private int IntellectSaveMod; // Модификатор интеллекта
    private int EnduranceSaveMod; // Модификатор выносливости
    private int WisdomSaveMod;    // Модификатор мудрости
    private int CharismaSaveMod;  // Модификатор харизмы

    // Владение спасбросками
    private boolean ForceSavePossession;     // Модификатор силы
    private boolean DexteritySavePossession; // Модификатор ловкости
    private boolean IntellectSavePossession; // Модификатор интеллекта
    private boolean EnduranceSavePossession; // Модификатор выносливости
    private boolean WisdomSavePossession;    // Модификатор мудрости
    private boolean CharismaSavePossession;  // Модификатор харизмы

    // Навыки
    private int acrobatic;      // Акробатика
    private int analise;        // Анализ
    private int athletics;      // Атлетика
    private int attentiveness;  // Внимательность
    private int survival;       // Выживание
    private int performance;    // Выступление
    private int intimidation;   // Запугивание
    private int history;        // История
    private int sleightOfHand;  // Ловкость рук
    private int magick;         // Магия
    private int medicine;       // Медицина
    private int deception;      // Обман
    private int nature;         // Природа
    private int insight;        // Проницательность
    private int religion;       // Религия
    private int secrecy;        // Скрытность
    private int belief;         // Убеждение
    private int animalCare;     // Уход за животными

    // Владение навыками
    private int numberOfFeaturesAvailable;    // Количество доступных характеристик
    private boolean acrobaticPossession;      // Акробатика
    private boolean analisePossession;        // Анализ
    private boolean athleticsPossession;      // Атлетика
    private boolean attentivenessPossession;  // Внимательность
    private boolean survivalPossession;       // Выживание
    private boolean performancePossession;    // Выступление
    private boolean intimidationPossession;   // Запугивание
    private boolean historyPossession;        // История
    private boolean sleightOfHandPossession;  // Ловкость рук
    private boolean magickPossession;         // Магия
    private boolean medicinePossession;       // Медицина
    private boolean deceptionPossession;      // Обман
    private boolean naturePossession;         // Природа
    private boolean insightPossession;        // Проницательность
    private boolean religionPossession;       // Религия
    private boolean secrecyPossession;        // Скрытность
    private boolean beliefPossession;         // Убеждение
    private boolean animalCarePossession;     // Уход за животными

    // Второстепенные пункты
    private int Inspiration;        // Вдохновение
    private int SkillBonus;         // Бонус мастерства
    private int PassiveMindfulness; // Пассивная внимательность
    private int InitiativeMod;      // Инициатива
    private int Speed;              // Скорость перемещения в футах (одна клетка = 5 футов)

    // Вооружение, доспехи и снаряжение
    private int ArmorClass;          // Класс доспехов
    private String ArmorName;        // Название доспехов
    private List<Weapon> Weapons;    // Набор оружия
    private Spell[] Spells;          // Набор заклинаний
    private String[] OtherItems;     // Другие предметы
    private Coins Coins;             // Монеты



    public CharacterList(){
        CharacterName = "";
        CharacterLevel = 1;
        ideology = "";
        Race = "";
        Class = "";
        Origin = "";
        LanguageProficiency = new ArrayList<>();
        for(int i=0; i<10; i++)
            LanguageProficiency.add(i, null);
        UniqueFeatures = "";
        BackStory = "";
        MaxHP = 0;
        DiceHP = "D6";
        Force = 1;
        Dexterity = 1;
        Intellect = 1;
        BodyType = 1;
        Wisdom = 1;
        Charisma = 1;

        SkillBonus = 2;

        numberOfFeaturesAvailable = 18;   // Количество доступных характеристик
        acrobaticPossession = false;      // Акробатика
        analisePossession = false;        // Анализ
        athleticsPossession = false;      // Атлетика
        attentivenessPossession = false;  // Внимательность
        survivalPossession = false;       // Выживание
        performancePossession = false;    // Выступление
        intimidationPossession = false;   // Запугивание
        historyPossession = false;        // История
        sleightOfHandPossession = false;  // Ловкость рук
        magickPossession = false;         // Магия
        medicinePossession = false;       // Медицина
        deceptionPossession = false;      // Обман
        naturePossession = false;         // Природа
        insightPossession = false;        // Проницательность
        religionPossession = false;       // Религия
        secrecyPossession = false;        // Скрытность
        beliefPossession = false;         // Убеждение
        animalCarePossession = false;     // Уход за животными

        Speed = 0;
        ArmorClass = 0;
        ArmorName = "none";
        Weapons = new ArrayList<>();
        for(int i=0; i<17; i++)
            Weapons.add(i, null);
        Spells = null;
        OtherItems = null;
        Coins = null;
    }

    public CharacterList(String characterName, int characterLevel,
                         String ideology, String race,
                         String aClass, String origin,
                         List<String> languageProficiency, String uniqueFeatures,
                         String backStory, int maxHP,
                         String diceHP, int force,
                         int dexterity, int intellect,
                         int endurance, int wisdom,
                         int charisma, int speed,
                         int armorClass, String armorName,
                         List<Weapon> weapons, Spell[] spells,
                         String[] otherItems, CharacterList.Coins coins) {
        CharacterName = characterName;
        CharacterLevel = characterLevel;
        this.ideology = ideology;
        Race = race;
        Class = aClass;
        Origin = origin;
        LanguageProficiency = languageProficiency;
        UniqueFeatures = uniqueFeatures;
        BackStory = backStory;
        MaxHP = maxHP;
        DiceHP = diceHP;
        Force = force;
        Dexterity = dexterity;
        Intellect = intellect;
        BodyType = endurance;
        Wisdom = wisdom;
        Charisma = charisma;
        Speed = speed;
        ArmorClass = armorClass;
        ArmorName = armorName;
        Weapons = weapons;
        Spells = spells;
        OtherItems = otherItems;
        Coins = coins;
    }

    public CharacterList(String cName, String cClass, String cRace, String cIdeology,
                         int cForce, int cDexterity, int cIntellect, int cEndurance, int cWisdom, int cCharisma, String uniqueFeatures){
        this.CharacterName = cName;
        this.Class = cClass;
        this.Race = cRace;
        this.ideology = cIdeology;
        this.Force = cForce;
        this.Dexterity = cDexterity;
        this.Intellect = cIntellect;
        this.BodyType = cEndurance;
        this.Wisdom = cWisdom;
        this.Charisma = cCharisma;
        this.UniqueFeatures = uniqueFeatures;
    }


    public boolean isWantToUpdate() {
        return wantToUpdate;
    }

    public void setWantToUpdate(boolean updated) {
        this.wantToUpdate = updated;
    }

    public String getRasFeatures() {
        return RasFeatures;
    }

    public void setRasFeatures(String rasFeatures) {
        RasFeatures = rasFeatures;
    }

    public int getNumberOfFeaturesAvailable() {
        return numberOfFeaturesAvailable;
    }

    public void setNumberOfFeaturesAvailable(int numberOfFeaturesAvailable) {
        this.numberOfFeaturesAvailable = numberOfFeaturesAvailable;
    }

    public boolean isForceSavePossession() {
        return ForceSavePossession;
    }

    public void setForceSavePossession(boolean forceSavePossession) {
        ForceSavePossession = forceSavePossession;
    }

    public boolean isDexteritySavePossession() {
        return DexteritySavePossession;
    }

    public void setDexteritySavePossession(boolean dexteritySavePossession) {
        DexteritySavePossession = dexteritySavePossession;
    }

    public boolean isIntellectSavePossession() {
        return IntellectSavePossession;
    }

    public void setIntellectSavePossession(boolean intellectSavePossession) {
        IntellectSavePossession = intellectSavePossession;
    }

    public boolean isBodyTypeSavePossession() {
        return EnduranceSavePossession;
    }

    public void setBodyTypeSavePossession(boolean enduranceSavePossession) {
        EnduranceSavePossession = enduranceSavePossession;
    }

    public boolean isWisdomSavePossession() {
        return WisdomSavePossession;
    }

    public void setWisdomSavePossession(boolean wisdomSavePossession) {
        WisdomSavePossession = wisdomSavePossession;
    }

    public boolean isCharismaSavePossession() {
        return CharismaSavePossession;
    }

    public void setCharismaSavePossession(boolean charismaSavePossession) {
        CharismaSavePossession = charismaSavePossession;
    }

    public boolean isAcrobaticPossession() { return acrobaticPossession; }

    public void setAcrobaticPossession(boolean acrobaticPossession) { this.acrobaticPossession = acrobaticPossession; }

    public boolean isAnalisePossession() { return analisePossession; }

    public void setAnalisePossession(boolean analisePossession) { this.analisePossession = analisePossession; }

    public boolean isAthleticsPossession() { return athleticsPossession; }

    public void setAthleticsPossession(boolean athleticsPossession) { this.athleticsPossession = athleticsPossession; }

    public boolean isAttentivenessPossession() { return attentivenessPossession; }

    public void setAttentivenessPossession(boolean attentivenessPossession) { this.attentivenessPossession = attentivenessPossession; }

    public boolean isSurvivalPossession() { return survivalPossession; }

    public void setSurvivalPossession(boolean survivalPossession) { this.survivalPossession = survivalPossession; }

    public boolean isPerformancePossession() { return performancePossession; }

    public void setPerformancePossession(boolean performancePossession) { this.performancePossession = performancePossession; }

    public boolean isIntimidationPossession() { return intimidationPossession; }

    public void setIntimidationPossession(boolean intimidationPossession) { this.intimidationPossession = intimidationPossession; }

    public boolean isHistoryPossession() { return historyPossession; }

    public void setHistoryPossession(boolean historyPossession) { this.historyPossession = historyPossession; }

    public boolean isSleightOfHandPossession() { return sleightOfHandPossession; }

    public void setSleightOfHandPossession(boolean sleightOfHandPossession) { this.sleightOfHandPossession = sleightOfHandPossession; }

    public boolean isMagickPossession() { return magickPossession; }

    public void setMagickPossession(boolean magickPossession) { this.magickPossession = magickPossession; }

    public boolean isMedicinePossession() { return medicinePossession; }

    public void setMedicinePossession(boolean medicinePossession) { this.medicinePossession = medicinePossession; }

    public boolean isDeceptionPossession() { return deceptionPossession; }

    public void setDeceptionPossession(boolean deceptionPossession) { this.deceptionPossession = deceptionPossession; }

    public boolean isNaturePossession() { return naturePossession; }

    public void setNaturePossession(boolean naturePossession) { this.naturePossession = naturePossession; }

    public boolean isInsightPossession() { return insightPossession; }

    public void setInsightPossession(boolean insightPossession) { this.insightPossession = insightPossession; }

    public boolean isReligionPossession() { return religionPossession; }

    public void setReligionPossession(boolean religionPossession) { this.religionPossession = religionPossession; }

    public boolean isSecrecyPossession() { return secrecyPossession; }

    public void setSecrecyPossession(boolean secrecyPossession) { this.secrecyPossession = secrecyPossession; }

    public boolean isBeliefPossession() { return beliefPossession; }

    public void setBeliefPossession(boolean beliefPossession) { this.beliefPossession = beliefPossession; }

    public boolean isAnimalCarePossession() { return animalCarePossession; }

    public void setAnimalCarePossession(boolean animalCarePossession) { this.animalCarePossession = animalCarePossession; }

    // Геттеры
    public String getCharacterName() {
        return CharacterName;
    }

    public int getCharacterLevel() {
        return CharacterLevel;
    }

    public String getRace() {
        return Race;
    }

    public String getCharacterClass() {
        return Class;
    }

    public String getOrigin() {
        return Origin;
    }

    public List<String> getLanguageProficiency() {
        return LanguageProficiency;
    }

    public String getUniqueFeatures() {
        return UniqueFeatures;
    }

    public String getBackStory() {
        return BackStory;
    }

    public int getCurrentHP() {
        return CurrentHP;
    }

    public int getMaxHP() {
        return MaxHP;
    }

    public String getDiceHP() {
        return DiceHP;
    }

    public int getForce() {
        return Force;
    }

    public int getDexterity() {
        return Dexterity;
    }

    public int getIntellect() {
        return Intellect;
    }

    public int getBodyType() {
        return BodyType;
    }

    public int getWisdom() {
        return Wisdom;
    }

    public int getCharisma() {
        return Charisma;
    }

    public int getForceMod() {
        return ForceMod;
    }

    public int getDexterityMod() {
        return DexterityMod;
    }

    public int getIntellectMod() {
        return IntellectMod;
    }

    public int getBodyTypeMod() {
        return EnduranceMod;
    }

    public int getWisdomMod() {
        return WisdomMod;
    }

    public int getCharismaMod() {
        return CharismaMod;
    }

    public int getEnduranceMod() {
        return EnduranceMod;
    }

    public int getForceSaveMod() {
        return ForceSaveMod;
    }

    public int getDexteritySaveMod() {
        return DexteritySaveMod;
    }

    public int getIntellectSaveMod() {
        return IntellectSaveMod;
    }

    public int getEnduranceSaveMod() {
        return EnduranceSaveMod;
    }

    public int getWisdomSaveMod() {
        return WisdomSaveMod;
    }

    public int getCharismaSaveMod() {
        return CharismaSaveMod;
    }

    public int getAcrobatic() {
        return acrobatic;
    }

    public int getAnalise() {
        return analise;
    }

    public int getAthletics() {
        return athletics;
    }

    public int getAttentiveness() {
        return attentiveness;
    }

    public int getSurvival() {
        return survival;
    }

    public int getPerformance() {
        return performance;
    }

    public int getIntimidation() {
        return intimidation;
    }

    public int getHistory() {
        return history;
    }

    public int getSleightOfHand() {
        return sleightOfHand;
    }

    public int getMagick() {
        return magick;
    }

    public int getMedicine() {
        return medicine;
    }

    public int getDeception() {
        return deception;
    }

    public int getNature() {
        return nature;
    }

    public int getInsight() {
        return insight;
    }

    public int getReligion() {
        return religion;
    }

    public int getSecrecy() {
        return secrecy;
    }

    public int getBelief() {
        return belief;
    }

    public int getAnimalCare() {
        return animalCare;
    }

    public int getSpeed() {
        return Speed;
    }

    public int getArmorClass() {
        return ArmorClass;
    }

    public String getArmorName() {
        return ArmorName;
    }

    public List<Weapon> getWeapons() {
        return Weapons;
    }

    public Spell[] getSpells() {
        return Spells;
    }

    public String[] getOtherItems() {
        return OtherItems;
    }

    public CharacterList.Coins getCoins() {
        return Coins;
    }

    public String getCharClass() {
        return Class;
    }

    public int getInspiration() {
        return Inspiration;
    }

    public int getSkillBonus() {
        return SkillBonus;
    }

    public int getPassiveMindfulness() {
        return PassiveMindfulness;
    }

    public int getInitiativeMod() {
        return InitiativeMod;
    }

    public String getIdeology() {
        return ideology;
    }

    // Сеттеры
    public void setCharacterName(String characterName){ CharacterName = characterName; }

    public void setCharacterLevel(int characterLevel) {
        CharacterLevel = characterLevel;
    }

    public void setRace(String race) {
        Race = race;
    }

    public void setClass(String aClass) {
        Class = aClass;
    }

    public void setOrigin(String origin) {
        Origin = origin;
    }

    public void setLanguageProficiency(List<String> languageProficiency) {
        LanguageProficiency = languageProficiency;
    }

    public void setUniqueFeatures(String uniqueFeatures) {
        UniqueFeatures = uniqueFeatures;
    }

    public void setBackStory(String backStory) {
        BackStory = backStory;
    }

    public void setCurrentHP(int currentHP) {
        CurrentHP = currentHP;
    }

    public void setMaxHP(int maxHP) {
        MaxHP = maxHP;
    }

    public void setDiceHP(String diceHP) {
        DiceHP = diceHP;
    }

    public void setForce(int force) {
        Force = force;
    }

    public void setDexterity(int dexterity) {
        Dexterity = dexterity;
    }

    public void setIntellect(int intellect) {
        Intellect = intellect;
    }

    public void setBodyType(int endurance) {
        BodyType = endurance;
    }

    public void setWisdom(int wisdom) {
        Wisdom = wisdom;
    }

    public void setCharisma(int charisma) {
        Charisma = charisma;
    }

    public void setForceMod(int forceMod) {
        ForceMod = forceMod;
    }

    public void setDexterityMod(int dexterityMod) {
        DexterityMod = dexterityMod;
    }

    public void setIntellectMod(int intellectMod) {
        IntellectMod = intellectMod;
    }

    public void setBodyTypeMod(int enduranceMod) {
        EnduranceMod = enduranceMod;
    }

    public void setWisdomMod(int wisdomMod) {
        WisdomMod = wisdomMod;
    }

    public void setCharismaMod(int charismaMod) {
        CharismaMod = charismaMod;
    }

    public void setForceSaveMod(int forceSaveMod) {
        ForceSaveMod = forceSaveMod;
    }

    public void setDexteritySaveMod(int dexteritySaveMod) {
        DexteritySaveMod = dexteritySaveMod;
    }

    public void setIntellectSaveMod(int intellectSaveMod) {
        IntellectSaveMod = intellectSaveMod;
    }

    public void setEnduranceSaveMod(int enduranceSaveMod) {
        EnduranceSaveMod = enduranceSaveMod;
    }

    public void setWisdomSaveMod(int wisdomSaveMod) {
        WisdomSaveMod = wisdomSaveMod;
    }

    public void setCharismaSaveMod(int charismaSaveMod) {
        CharismaSaveMod = charismaSaveMod;
    }

    public void setAcrobatic(int acrobatic) {
        this.acrobatic = acrobatic;
    }

    public void setAnalise(int analise) {
        this.analise = analise;
    }

    public void setAthletics(int athletics) {
        this.athletics = athletics;
    }

    public void setAttentiveness(int attentiveness) {
        this.attentiveness = attentiveness;
    }

    public void setSurvival(int survival) {
        this.survival = survival;
    }

    public void setPerformance(int performance) {
        this.performance = performance;
    }

    public void setIntimidation(int intimidation) {
        this.intimidation = intimidation;
    }

    public void setHistory(int history) {
        this.history = history;
    }

    public void setSleightOfHand(int sleightOfHand) {
        this.sleightOfHand = sleightOfHand;
    }

    public void setMagick(int magick) {
        this.magick = magick;
    }

    public void setMedicine(int medicine) {
        this.medicine = medicine;
    }

    public void setDeception(int deception) {
        this.deception = deception;
    }

    public void setNature(int nature) {
        this.nature = nature;
    }

    public void setInsight(int insight) {
        this.insight = insight;
    }

    public void setReligion(int religion) {
        this.religion = religion;
    }

    public void setSecrecy(int secrecy) {
        this.secrecy = secrecy;
    }

    public void setBelief(int belief) {
        this.belief = belief;
    }

    public void setAnimalCare(int animalCare) {
        this.animalCare = animalCare;
    }

    public void setSkillBonus(int skillBonus) {
        SkillBonus = skillBonus;
    }

    public void setPassiveMindfulness(int passiveMindfulness) {
        PassiveMindfulness = passiveMindfulness;
    }

    public void setInitiativeMod(int initiativeMod) {
        InitiativeMod = initiativeMod;
    }

    public void setInspiration(int inspiration) {
        Inspiration = inspiration;
    }

    public void setSpeed(int speed) {
        Speed = speed;
    }

    public void setArmorClass(int armorClass) {
        ArmorClass = armorClass;
    }

    public void setArmorName(String armorName) {
        ArmorName = armorName;
    }

    public void setWeapons(List<Weapon> weapons) {
        Weapons = weapons;
    }

    public void setSpells(Spell[] spells) {
        Spells = spells;
    }

    public void setOtherItems(String[] otherItems) {
        OtherItems = otherItems;
    }

    public void setCoins(CharacterList.Coins coins) {
        Coins = coins;
    }

    public void setIdeology(String ideology) {
        this.ideology = ideology;
    }




    // Класс для описания оружия
    public static class Weapon{
        private boolean MastersBonus;
        private String WeaponName;
        private int Damage;
        private String DamageDice;


        public Weapon(String weaponName, int damage, String damageDice, boolean mastersBonus) {
            WeaponName = weaponName;
            Damage = damage;
            DamageDice = damageDice;
            MastersBonus = mastersBonus;
        }


        public String getWeaponName() { return WeaponName; }
        public int getDamage() { return Damage; }
        public String getDamageDice() { return DamageDice; }
        public boolean getMastersBonus(){ return MastersBonus; }
    }

    // Класс для описания заклинаний
    public class Spell{
        private String SpellName;
        private int TimeOfCreation;
        private String SpellType;
        private int Damage;
        private String DamageDice;
        private boolean MastersBonus;

        public Spell(String spellName, int timeOfCreation, boolean mastersBonus, String spellType){
            SpellName = spellName;
            TimeOfCreation = timeOfCreation;
            MastersBonus = mastersBonus;
            SpellType = spellType;
            Damage = 0;
            DamageDice = null;
        }

        public Spell(String spellName, int timeOfCreation, boolean mastersBonus, String spellType, int damage, String damageDice) {
            SpellName = spellName;
            TimeOfCreation = timeOfCreation;
            MastersBonus = mastersBonus;
            SpellType = spellType;
            Damage = damage;
            DamageDice = damageDice;
        }

        // Геттеры
        public String getSpellName() { return SpellName; }
        public int getTimeOfCreation() { return TimeOfCreation; }
        public boolean getMastersBonus(){ return MastersBonus; }
        public String getSpellType() { return SpellType; }
        public int getDamage() { return Damage; }
        public String getDamageDice() { return DamageDice; }
    }

    // Класс для хранения монет
    public class Coins{
        private int CopperCoins;
        private int SilverCoins;
        private int GoldenCoins;
        private int PlatinumCoins;

        // Геттеры
        public int getCopperCoins() { return CopperCoins; }
        public int getPlatinumCoins() { return PlatinumCoins; }
        public int getSilverCoins() { return SilverCoins; }
        public int getGoldenCoins() { return GoldenCoins; }

        // Сеттеры
        public void setCopperCoins(int copperCoins) {
            CopperCoins = copperCoins;
        }
        public void setSilverCoins(int silverCoins) {
            SilverCoins = silverCoins;
        }
        public void setGoldenCoins(int goldenCoins) {
            GoldenCoins = goldenCoins;
        }
        public void setPlatinumCoins(int platinumCoins) {
            PlatinumCoins = platinumCoins;
        }
    }

//    private int Force;     // Сила
//    private int Dexterity; // Ловкость
//    private int Intellect; // Интеллект
//    private int BodyType/endurance;  // Телосложение
//    private int Wisdom;    // Мудрость
//    private int Charisma;  // Харизма

    public CharacterList compileCharacterList(){

        setForceMod(Utilities.getMods(getForce()));
        setDexterityMod(Utilities.getMods(getDexterity()));
        setIntellectMod(Utilities.getMods(getIntellect()));
        setBodyTypeMod(Utilities.getMods(getBodyType()));
        setWisdomMod(Utilities.getMods(getWisdom()));
        setCharismaMod(Utilities.getMods(getCharisma()));

        setForceSaveMod(isForceSavePossession() ? getForceMod()+getSkillBonus() : getForceMod());
        setDexteritySaveMod(isDexteritySavePossession() ? getDexterityMod()+getSkillBonus() : getDexterityMod());
        setIntellectSaveMod(isIntellectSavePossession() ? getIntellectMod()+getSkillBonus() : getIntellectMod());
        setEnduranceSaveMod(isBodyTypeSavePossession() ? getBodyTypeMod()+getSkillBonus() : getBodyTypeMod());
        setWisdomSaveMod(isWisdomSavePossession() ? getWisdomMod()+getSkillBonus() : getWisdomMod());
        setCharismaSaveMod(isCharismaSavePossession() ? getCharismaMod()+getSkillBonus() : getCharismaMod());

        setAcrobatic(isAcrobaticPossession() ? getDexterityMod()+getSkillBonus() : getDexterityMod());
        setAnalise(isAnalisePossession() ? getIntellectMod()+getSkillBonus() : getIntellectMod());
        setAthletics(isAthleticsPossession() ? getForceMod()+getSkillBonus() : getForceMod());
        setAttentiveness(isAttentivenessPossession() ? getWisdomMod()+getSkillBonus() : getWisdomMod());
        setSurvival(isSurvivalPossession() ? getWisdomMod()+getSkillBonus() : getWisdomMod());
        setPerformance(isPerformancePossession() ? getCharismaMod()+getSkillBonus() : getCharismaMod());
        setIntimidation(isIntimidationPossession() ? getCharismaMod()+getSkillBonus() : getCharismaMod());
        setHistory(isHistoryPossession() ? getIntellectMod()+getSkillBonus() : getIntellectMod());
        setSleightOfHand(isSleightOfHandPossession() ? getDexterityMod()+getSkillBonus() : getDexterityMod());
        setMagick(isMagickPossession() ? getIntellectMod()+getSkillBonus() : getIntellectMod());
        setMedicine(isMedicinePossession() ? getWisdomMod()+getSkillBonus() : getWisdomMod());
        setDeception(isDeceptionPossession() ? getCharismaMod()+getSkillBonus() : getCharismaMod());
        setNature(isNaturePossession() ? getIntellectMod()+getSkillBonus() : getIntellectMod());
        setInsight(isInsightPossession() ? getWisdomMod()+getSkillBonus() : getWisdomMod());
        setReligion(isReligionPossession() ? getIntellectMod()+getSkillBonus() : getIntellectMod());
        setSecrecy(isSecrecyPossession() ? getDexterityMod()+getSkillBonus() : getDexterityMod());
        setBelief(isBeliefPossession() ? getCharismaMod()+getSkillBonus() : getCharismaMod());
        setAnimalCare(isAnimalCarePossession() ? getWisdomMod()+getSkillBonus() : getWisdomMod());

        // Установка класса доспеха
        switch(getArmorName()){
            case "Стёганный доспех":
                setArmorClass(11+getDexterityMod());
                break;
            case "Кожаный доспех":
                setArmorClass(11+getDexterityMod());
                break;
            case "Клёпанный доспех":
                setArmorClass(12+getDexterityMod());
                break;
            case "Доспехи из шкуры":
                setArmorClass(12+getDexterityMod());
                break;
            case "Кольчуга":
                setArmorClass(13+getDexterityMod());
                break;
            case "Чешуйчатый доспех":
                setArmorClass(14+getDexterityMod());
                break;
            case "Кираса":
                setArmorClass(14+getDexterityMod());
                break;
            case "Полулаты":
                setArmorClass(15+getDexterityMod());
                break;
            case "Колечный доспех":
                setArmorClass(14);
                break;
            case "Кольчужный доспех":
                setArmorClass(16);
                break;
            case "Неполный доспех":
                setArmorClass(17);
                break;
            case "Полные латы":
                setArmorClass(18);
                break;
        }

        setPassiveMindfulness(getWisdomSaveMod());

        setCurrentHP(getMaxHP());



        return this;
    }
}
