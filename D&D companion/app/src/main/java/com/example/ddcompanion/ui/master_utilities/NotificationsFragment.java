package com.example.ddcompanion.ui.master_utilities;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ddcompanion.CharacterList;
import com.example.ddcompanion.MainActivity;
import com.example.ddcompanion.adapters.EnemyAdapter;
import com.example.ddcompanion.R;
import com.example.ddcompanion.Utilities;
import com.example.ddcompanion.adapters.MasterSaveAdapter;
import com.example.ddcompanion.databinding.FragmentNotificationsBinding;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class NotificationsFragment extends Fragment {

    private FragmentNotificationsBinding binding;

    // testBlockStart v1.91
    private String listLocation = "";
    private String listNPC = "";
    private String listQuest = "";
    // testBlockEnd v1.91

    private RecyclerView enemyList;
    private ListView companyList;

    private ArrayList<List<String>> arrayList;

    private List<CharacterList> enemyCharList;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentNotificationsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        enemyCharList = new ArrayList<>();
        initList();

        final ArrayList<ViewGroup> saves  = new ArrayList<>();
        final ArrayAdapter<ViewGroup> saveAdapter = new ArrayAdapter<>(getContext(), R.layout.short_enemy_info_layout, saves);

        final ArrayList<ViewGroup> enemy = new ArrayList<>();
        final ArrayAdapter<ViewGroup> adapter = new ArrayAdapter<>(getContext(), R.layout.enemy_element, enemy);

        final ArrayList<ViewGroup> company = new ArrayList<>();


        // testBlockStart v1.91
        binding.addLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = Utilities.createPlacesMenu(getContext(), 0, null, null, null);
                Button yes = dialog.findViewById(R.id.yesButton);
                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EditText tv = (EditText) dialog.findViewById(R.id.locName);
                        EditText et = dialog.findViewById(R.id.duration);
                        listLocation += tv.getText().toString() + "\n\t\t\t" + et.getText().toString() + "\n\n";
                        binding.textLocations.setText(listLocation);
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        binding.addNpcButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = Utilities.createPlacesMenu(getContext(), 0, "Новый НПС", "Имя персонажа", "Описание персонажа");
                Button yes = dialog.findViewById(R.id.yesButton);
                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EditText tv = (EditText) dialog.findViewById(R.id.locName);
                        EditText et = dialog.findViewById(R.id.duration);
                        listNPC += tv.getText().toString() + "\n\t\t\t" + et.getText().toString() + "\n\n";
                        binding.textNPC.setText(listNPC);
                        dialog.dismiss();
                    }
                });
                dialog.show();
//                itemsNpc.add("new NPC");
//                npcView.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, itemsNpc));
            }
        });
        binding.addQuestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                itemsQuest.add("new quest");
//                questView.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, itemsQuest));

                Dialog dialog = Utilities.createPlacesMenu(getContext(), 0, "Новое задание", "Название задания", "Суть задания");
                Button yes = dialog.findViewById(R.id.yesButton);
                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EditText tv = (EditText) dialog.findViewById(R.id.locName);
                        EditText et = dialog.findViewById(R.id.duration);
                        listQuest += tv.getText().toString() + "\n\t\t\t" + et.getText().toString() + "\n\n";
                        binding.textQuests.setText(listQuest);
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        // testBlockEnd v1.91


        // Заполнение списка данными
        enemyList = binding.EnemyList;
        enemyList.setLayoutManager(new LinearLayoutManager(getContext()));
        enemyList.setAdapter(new EnemyAdapter(enemyCharList));

        binding.masterSaves.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.masterSaves.setAdapter(new MasterSaveAdapter(saves()));

        // Слушатели
        binding.companyCreator.setOnClickListener(listener);
        binding.enemyConstructor.setOnClickListener(listener);

        binding.newEnemy.setOnClickListener(listener);

        binding.menuButton.setOnClickListener(listener);
        binding.SaLmenuButton.setOnClickListener(listener);

        // Установка слушателей для кубиков
        binding.throwD4.setOnClickListener(diceListener);
        binding.throwD6.setOnClickListener(diceListener);
        binding.throwD10.setOnClickListener(diceListener);
        binding.throwD12.setOnClickListener(diceListener);
        binding.throwD20.setOnClickListener(diceListener);
        binding.throwD100.setOnClickListener(diceListener);

        return root;
    }

    // Заполнение полей врагов
    private void initList(){

        CharacterList cl  = new CharacterList("Имя", "Класс", "Раса", "Мировоззрение",
                0, 0, 0, 0, 0, 0,
                "Какая-то дополнительная информация.\n" +
                "Например, враг имеет мечь, наносящий 1D6 урона\n" +
                "и при удачной проверке магии ещё 1D6 урона огнём.");
        //enemyCharList.add(cl);

        cl  = new CharacterList("Бандит с тракта", "Класс", "Человек", "Хаотичный-\nзлой",
                16, 6, 14, 14, 12, 12,
                "Особая способность: Спланированная засада - " +
                        "игроки и другие персонажи не могут предвидеть " +
                        "нападение, если не пройдут проверку мастера " +
                        "или не обнаружат врага при помощи магии. " +
                        "Подобное нападение накладывает на один раунд ошеломление " +
                        "на весь отряд, а отаки бандита проходят с преимуществом.");
//        enemyCharList.add(cl);

        cl = new CharacterList("Гоблин-воин", "Варвар", "Гоблин", "Нейтральный-злой",
                17, 19, 16, 16, 14, 11,
                "Имеет кожаные доспехи со вставками из костей (КД - 11). " +
                        "");
//        enemyCharList.add(cl);

        cl = new CharacterList("Капитан стражи", "Паладин", "Человек", "Законопослущный-\nнейтральный",
                19,14,13,19,13,16,
                "Имеет неполные латные доспехи (КД - 17) + щит (КД - 2). " +
                        "Имеет при себе длинный мечь (1D8 рубящего урона). " +
                        "Владеет заклинанием \"Сокрушение зла\", которое наносит 1D4 урона " +
                        "и на три раунда позволяет Кпитану стражи наносить дополнительные 1D4 урона.");
//        enemyCharList.add(cl);

        cl = new CharacterList("Городской стражник", "Воин", "Человек", "Законопослушный-\nдобрый",
                18,10,14,12,11,10,
                "Может принимать состояние \"Приказ\", при котором все его проверки проходят с преимуществом (+2). " +
                        "Имеет кирасу (КД - 14) и короткий мечь (1D6 урона).");
//        enemyCharList.add(cl);

        cl = new CharacterList("Демон", "Чародей", "Нежить", "Хаотичный-злой",
                8,18,18,15,20,12,
                "Не имеет оружия, но может атаковать когтями (1D4 режущего урона) + " +
                        "при неудачном спас-броске игрока на телосложение ещё накладывает состояние отравления. " +
                        "Одет демон в легкую тканевую накидку (КД - 11). " +
                        "Демон с преимуществом творит заклинания \"Смена обличия\", \"Ужас\" и заклинания стихии огня. " +
                        "Демон имеет сопротивление к стихийным атакам.");
//        enemyCharList.add(cl);
    }

//    private List<String> criminalList(){
//        List<String> list = new ArrayList<>();
//
//        List<String> l = Utilities.getSkillsList(16, 6, 14, 14, 12, 12);
//
//        list.add(0, "Бандит с тракта");
//        list.add(1, "Воин");
//        list.add(2, "Человек");
//        list.add(3, "Хаотичный-\nзлой");
//        list.add(4, l.get(0));  // 6, 14, 12, 14, 12, 16
//        list.add(5, l.get(1));
//        list.add(6, l.get(2));
//        list.add(7, l.get(3));
//        list.add(8, l.get(4));
//        list.add(9, l.get(5));
//        list.add(10, "");
//
//        return list;
//    }
//    private List<String> goblinList(){
//        List<String> list = new ArrayList<>();
//
//        List<String> l = Utilities.getSkillsList(17, 19, 16, 16, 14, 11);
//
//        list.add(0, "Гоблин-воин");
//        list.add(1, "Варвар");
//        list.add(2, "Гоблин");
//        list.add(3, "Нейтральный-злой");
//        list.add(4, l.get(0));
//        list.add(5, l.get(1));
//        list.add(6, l.get(2));
//        list.add(7, l.get(3));
//        list.add(8, l.get(4));
//        list.add(9, l.get(5));
//        list.add(10, "");
//
//        return list;
//    }
//    private List<String> capitanGuardList(){
//        List<String> list = new ArrayList<>();
//
//        List<String> l = Utilities.getSkillsList(19, 14, 13, 19, 13, 16); // 14, 19, 13, 16, 13, 19
//
//        list.add(0, "Капитан стражи");
//        list.add(1, "Паладин");
//        list.add(2, "Человек");
//        list.add(3, "Законопослущный-\nнейтральный");
//        list.add(4, l.get(0));
//        list.add(5, l.get(1));
//        list.add(6, l.get(2));
//        list.add(7, l.get(3));
//        list.add(8, l.get(4));
//        list.add(9, l.get(5));
//        list.add(10, "");
//
//        return list;
//    }
//    private List<String> guardList(){
//        List<String> list = new ArrayList<>();
//
//        List<String> l = Utilities.getSkillsList(18, 10, 14, 12, 11, 10); // 14, 10, 11, 12, 18, 10
//
//        list.add(0, "Городской стражник");
//        list.add(1, "Воин");
//        list.add(2, "Человек");
//        list.add(3, "Законопослушный-\nдобрый");
//        list.add(4, l.get(0));
//        list.add(5, l.get(1));
//        list.add(6, l.get(2));
//        list.add(7, l.get(3));
//        list.add(8, l.get(4));
//        list.add(9, l.get(5));
//        list.add(10, "");
//
//        return list;
//    }
//    private List<String> daemonList(){
//        List<String> list = new ArrayList<>();
//
//        List<String> l = Utilities.getSkillsList(8, 18, 18, 15, 20, 12); // 20, 18, 8, 15, 18, 12
//
//        list.add(0, "Демон");
//        list.add(1, "Чародей");
//        list.add(2, "Нежить");
//        list.add(3, "Хаотичный-злой");
//        list.add(4, l.get(0));
//        list.add(5, l.get(1));
//        list.add(6, l.get(2));
//        list.add(7, l.get(3));
//        list.add(8, l.get(4));
//        list.add(9, l.get(5));
//        list.add(10, "");
//
//        return list;
//    }
//
//    private ArrayList<List<String>> arrayList(){
//        ArrayList<List<String>> a = new ArrayList<>();
//        a.add(initList());
//        a.add(criminalList());
//        a.add(goblinList());
//        a.add(capitanGuardList());
//        a.add(guardList());
//        a.add(daemonList());
//        return a;
//    }

    private List<String> saves(){
        List<String> list = new ArrayList<>();

        list.add("Город на краю");
        list.add("Дорога в Альто");
        list.add("Северная война");
        list.add("Забытые земли");

        return list;
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.companyCreator:
                    Utilities.animate(binding.companyCreatorMenu);
                    break;
                case R.id.enemyConstructor:
                    Utilities.animate(binding.enemyConstructorMenu);
                    break;
                case R.id.newEnemy:
                    Utilities.createNewEnemy(getContext(), enemyCharList, enemyList).show();
                    break;
                case R.id.menuButton:
                    if(binding.SaLMenu.getVisibility() != View.GONE)
                        Utilities.animate(binding.SaLMenu);
                    Utilities.animate(binding.rightMenu);
                    break;
                case R.id.SaLmenuButton:
                    if(binding.rightMenu.getVisibility() != View.GONE)
                        Utilities.animate(binding.rightMenu);
                    Utilities.animate(binding.SaLMenu);
                    break;
            }
        }
    };

    // Обработка кнопок для кубиков
    View.OnClickListener diceListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.throwD4:
                    binding.diceText.setText(Utilities.rdm(1, 5));
                    break;
                case R.id.throwD6:
                    binding.diceText.setText(Utilities.rdm(1, 7));
                    break;
                case R.id.throwD10:
                    binding.diceText.setText(Utilities.rdm(1, 11));
                    break;
                case R.id.throwD12:
                    binding.diceText.setText(Utilities.rdm(1, 13));
                    break;
                case R.id.throwD20:
                    binding.diceText.setText(Utilities.rdm(1, 21));
                    break;
                case R.id.throwD100:
                    binding.diceText.setText(Utilities.rdm(1, 101));
                    break;
                default:
                    binding.diceText.setText("Error");
            }
        }
    };



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}