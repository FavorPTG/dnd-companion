package com.example.ddcompanion.forCharacter;

import android.util.Pair;

import java.util.List;

public class CharacterClass {

    private int level;                  // Уровень персонажа
    private String diceHP;              // Кости хотов
    private String mainCharacteristic;  // Главные характеристики
    private String savingTrow;          // Владение спасбросками
    private String possession;          // Владения доспехами и оружием

    private List<Pair<String, String>> capabilities;  // Способности (название и описание)

    private String equipment;           // Снаряжение




    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getDiceHP() {
        return diceHP;
    }

    public void setDiceHP(String diceHP) {
        this.diceHP = diceHP;
    }

    public String getMainCharacteristic() {
        return mainCharacteristic;
    }

    public void setMainCharacteristic(String mainCharacteristic) {
        this.mainCharacteristic = mainCharacteristic;
    }

    public String getSavingTrow() {
        return savingTrow;
    }

    public void setSavingTrow(String savingTrow) {
        this.savingTrow = savingTrow;
    }

    public String getPossession() {
        return possession;
    }

    public void setPossession(String possession) {
        this.possession = possession;
    }

    public List<Pair<String, String>> getCapabilities() {
        return capabilities;
    }

    public void setCapabilities(List<Pair<String, String>> capabilities) {
        this.capabilities = capabilities;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }
}
