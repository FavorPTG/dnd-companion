package com.example.ddcompanion.forCharacter;

import android.content.Context;
import android.util.Pair;

import com.example.ddcompanion.R;

import java.util.List;

public class ClassBard extends CharacterClass {

    Context context;

    public ClassBard(Context context){
        this.context = context;
    }

    private final String className = "Бард";                               // Название класса
    private final String classDescription =
            context.getResources().getString(R.string.class_bard_desc);    // Описание класса

    private int countOfCells1;  // Количество ячеек первого уровня
    private int countOfCells2;  // Количество ячеек второго уровня
    private int countOfCells3;  // Количество ячеек третьего уровня
    private int countOfCells4;  // Количество ячеек четвёртого уровня
    private int countOfCells5;  // Количество ячеек пятого уровня
    private int countOfCells6;  // Количество ячеек шестого уровня
    private int countOfCells7;  // Количество ячеек седьмого уровня
    private int countOfCells8;  // Количество ячеек восьмого уровня
    private int countOfCells9;  // Количество ячеек девятого уровня

    private List<Pair<String, String>> famousConspiracies;  // Известные заговоры (название + описание)
    private List<Pair<String, String>> famousSpells;        // Известные заклинания (название + описание)

    public int getCountOfCells1() {
        return countOfCells1;
    }

    public void setCountOfCells1(int countOfCells1) {
        this.countOfCells1 = countOfCells1;
    }

    public int getCountOfCells2() {
        return countOfCells2;
    }

    public void setCountOfCells2(int countOfCells2) {
        this.countOfCells2 = countOfCells2;
    }

    public int getCountOfCells3() {
        return countOfCells3;
    }

    public void setCountOfCells3(int countOfCells3) {
        this.countOfCells3 = countOfCells3;
    }

    public int getCountOfCells4() {
        return countOfCells4;
    }

    public void setCountOfCells4(int countOfCells4) {
        this.countOfCells4 = countOfCells4;
    }

    public int getCountOfCells5() {
        return countOfCells5;
    }

    public void setCountOfCells5(int countOfCells5) {
        this.countOfCells5 = countOfCells5;
    }

    public int getCountOfCells6() {
        return countOfCells6;
    }

    public void setCountOfCells6(int countOfCells6) {
        this.countOfCells6 = countOfCells6;
    }

    public int getCountOfCells7() {
        return countOfCells7;
    }

    public void setCountOfCells7(int countOfCells7) {
        this.countOfCells7 = countOfCells7;
    }

    public int getCountOfCells8() {
        return countOfCells8;
    }

    public void setCountOfCells8(int countOfCells8) {
        this.countOfCells8 = countOfCells8;
    }

    public int getCountOfCells9() {
        return countOfCells9;
    }

    public void setCountOfCells9(int countOfCells9) {
        this.countOfCells9 = countOfCells9;
    }

    public List<Pair<String, String>> getFamousConspiracies() {
        return famousConspiracies;
    }

    public void setFamousConspiracies(List<Pair<String, String>> famousConspiracies) {
        this.famousConspiracies = famousConspiracies;
    }

    public List<Pair<String, String>> getFamousSpells() {
        return famousSpells;
    }

    public void setFamousSpells(List<Pair<String, String>> famousSpells) {
        this.famousSpells = famousSpells;
    }
}
