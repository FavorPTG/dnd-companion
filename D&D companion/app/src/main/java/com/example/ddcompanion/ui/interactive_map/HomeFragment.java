package com.example.ddcompanion.ui.interactive_map;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.ddcompanion.R;
import com.example.ddcompanion.Utilities;
import com.example.ddcompanion.databinding.FragmentHomeBinding;

public class HomeFragment extends Fragment {

    private FragmentHomeBinding binding;
    private GridLayout grid;
    private GridLayout coinsGrid;
    private View activeView;



    private boolean isActive = false;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        grid = binding.gridLayout;
        coinsGrid = binding.coins;

        Drawable cell_map = getContext().getDrawable(R.drawable.custom_cell_map);
        Drawable cell_map_active = getContext().getDrawable(R.drawable.custom_cell_map_active);


        // TODO: Очень странно, но при перемещении жетона на карту его картинка в меню уменьшается
        for(int i=0; i<grid.getChildCount(); i++){
            grid.getChildAt(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isActive && (activeView.getTag() == null) && (v.getForeground() == null)){
                        activeView.setBackground(cell_map);
                        v.setForeground(activeView.getForeground());
                        activeView.setForeground(null);
                        isActive = false;
                    } else if(isActive && (activeView.getTag() == null) && (v.getForeground() != null)){
                        activeView.setBackground(cell_map);
                        isActive = false;
                    } else if(isActive && (activeView.getTag() != null) && (v.getForeground() == null)){
                        v.setForeground(activeView.getForeground());
                        activeView.setBackground(cell_map);
                        isActive = false;
                    } else if(isActive && (activeView.getTag() != null) && (v.getForeground() != null)){
                        activeView.setBackground(cell_map);
                        isActive = false;
                    } else if(!isActive) {
                        if (v.getForeground() != null) {
                            v.setBackground(cell_map_active);
                            activeView = v;
                            isActive = true;
                        }
                    }
                }
            });
        }

        for(int i=0; i<coinsGrid.getChildCount(); i++){
            coinsGrid.getChildAt(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isActive && (v.getId() != R.id.delete_coin)){
                        activeView.setBackground(cell_map);
                        isActive = false;
                    } else if(isActive && (v.getId() == R.id.delete_coin) && (activeView.getTag() == null)){
                        activeView.setBackground(cell_map);
                        activeView.setForeground(null);
                        isActive = false;
                    } else if(!isActive && v.getId() != R.id.delete_coin){
                        v.setBackground(cell_map_active);
                        activeView = v;
                        isActive = true;
                    }
                }
            });
        }



        binding.closeButton.setOnClickListener(listener);

        binding.back1.setOnClickListener(listener);
        binding.back2.setOnClickListener(listener);
        binding.back3.setOnClickListener(listener);
        binding.back4.setOnClickListener(listener);
        binding.back5.setOnClickListener(listener);
        binding.back6.setOnClickListener(listener);
        binding.back7.setOnClickListener(listener);
        binding.back8.setOnClickListener(listener);
        binding.back9.setOnClickListener(listener);
        binding.back10.setOnClickListener(listener);
        binding.back11.setOnClickListener(listener);
        binding.back12.setOnClickListener(listener);
        binding.back13.setOnClickListener(listener);
        binding.back14.setOnClickListener(listener);
        binding.back15.setOnClickListener(listener);
        binding.back16.setOnClickListener(listener);
        binding.back17.setOnClickListener(listener);
        binding.back18.setOnClickListener(listener);
        binding.back19.setOnClickListener(listener);
        binding.back20.setOnClickListener(listener);

        return root;
    }

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.close_button:
                    Utilities.animate(binding.leftFadeoutMenu);
                    break;
                case R.id.back1:
                    binding.background.setImageResource(R.drawable.back_map_1);
                    break;
                case R.id.back2:
                    binding.background.setImageResource(R.drawable.back_map_2);
                    break;
                case R.id.back3:
                    binding.background.setImageResource(R.drawable.back_map_3);
                    break;
                case R.id.back4:
                    binding.background.setImageResource(R.drawable.back_map_4);
                    break;
                case R.id.back5:
                    binding.background.setImageResource(R.drawable.back_map_5);
                    break;
                case R.id.back6:
                    binding.background.setImageResource(R.drawable.back_map_6);
                    break;
                case R.id.back7:
                    binding.background.setImageResource(R.drawable.back_map_7);
                    break;
                case R.id.back8:
                    binding.background.setImageResource(R.drawable.back_map_8);
                    break;
                case R.id.back9:
                    binding.background.setImageResource(R.drawable.back_map_9);
                    break;
                case R.id.back10:
                    binding.background.setImageResource(R.drawable.back_map_10);
                    break;
                case R.id.back11:
                    binding.background.setImageResource(R.drawable.back_map_11);
                    break;
                case R.id.back12:
                    binding.background.setImageResource(R.drawable.back_map_12);
                    break;
                case R.id.back13:
                    binding.background.setImageResource(R.drawable.back_map_13);
                    break;
                case R.id.back14:
                    binding.background.setImageResource(R.drawable.back_map_14);
                    break;
                case R.id.back15:
                    binding.background.setImageResource(R.drawable.back_map_15);
                    break;
                case R.id.back16:
                    binding.background.setImageResource(R.drawable.back_map_16);
                    break;
                case R.id.back17:
                    binding.background.setImageResource(R.drawable.back_map_17);
                    break;
                case R.id.back18:
                    binding.background.setImageResource(R.drawable.back_map_18);
                    break;
                case R.id.back19:
                    binding.background.setImageResource(R.drawable.back_map_19);
                    break;
                case R.id.back20:
                    binding.background.setImageResource(R.drawable.back_map_20);
                    break;
            }
        }
    };




    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}