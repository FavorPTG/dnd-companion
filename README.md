<p><h1>Приложение-компаньон для D&D</h1></p>
<br>

<p><h3>Основные требования</h3></p>

1. Наличие сохраняемого листа персонажа.
2. Наличие интерактивной настраиваемой карты (с возможностью размещения на ней жетонов).
3. Наличие пошагового редактора приключений для мастера.

<p><h3>Предположительное размещение экранов</h3></p>

Начальный экран - интерактивная карта.<br>
Второй экран - лист персонажа.<br>
Третий экран - инструменты мастера.<br>

<p><h3>Скриншоты:</h3></p>

![alt text](Screenshots/Screenshot_20230528-181829_D&D companion.jpg "Title"){width=40%}
![alt text](Screenshots/Screenshot_20230528-181857_D&D companion.jpg "Title"){width=40%} <br>
![alt text](Screenshots/Screenshot_20230528-181906_D&D companion.jpg "Title"){width=40%}
![alt text](Screenshots/Screenshot_20230528-181921_D&D companion.jpg "Title"){width=40%} <br>
![alt text](Screenshots/Screenshot_20230528-181951_D&D companion.jpg "Title"){width=40%}
![alt text](Screenshots/Screenshot_20230528-182005_D&D companion.jpg "Title"){width=40%} <br>
![alt text](Screenshots/Screenshot_20230528-182011_D&D companion.jpg "Title"){width=40%}
![alt text](Screenshots/Screenshot_20230528-182020_D&D companion.jpg "Title"){width=40%} <br>
![alt text](Screenshots/Screenshot_20230528-182028_D&D companion.jpg "Title"){width=40%}
![alt text](Screenshots/Screenshot_20230528-182246_D&D companion.jpg "Title"){width=40%} <br>
![alt text](Screenshots/Screenshot_20230528-182427_D&D companion.jpg "Title"){width=40%}
![alt text](Screenshots/Screenshot_20230528-182438_D&D companion.jpg "Title"){width=40%} <br>
![alt text](Screenshots/Screenshot_20230528-182445_D&D companion.jpg "Title"){width=40%}

<p><h3>История изменений</h3></p>
Версия 1.8: <br>
Остановлена работа над интерактивной картой. На соответствующем экране имеется сама карта с клетками и боковое меню с возможностью выбора фона карты и жетонов.<br>
Сделан инструмент создания и вывода на экран кастомных диалоговых окон. Одно из таких уже спрятано в приложении ;)<br>
Почти завершена работа над методами для чтения/редактирования баз данных.<br>
<br>
Версия 1.9: <br>
Проведена работа по функциям для создания врагов на экране инструментов мастера. Начат перенос меню сохранения/загрузки в отдельные боковые меню. Реализованы несколько разнообразных диалоговых окон. <br>
Продолжается работа над инструментами работы с базами данных. <br>
<br>
Версия 1.91: <br>
Остановлена работа над функционалом инструментов мастера. Всё, что планировалось реализовать - реализовано. Начата работа над дополнительными классами для описания классов и рас персонажей. Доработывается класс листа персонажа. <br>
